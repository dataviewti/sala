import Vue from 'vue'
import lodash from 'lodash'
import axios from 'axios'
import vuetify from './plugins/vuetify'
import './helpers/z-prototypes'

require('./bootstrap')

globalThis._ = lodash
globalThis.Vue = Vue
globalThis.axios = axios
globalThis.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'

Vue.component('components/fe/z-teste.vue')

// eslint-disable-next-line no-unused-vars
const app = new Vue({
  vuetify,
  el: '#app-fe'
})
