import BaseComponent from './BaseComponent'
import * as colors from '~/helpers/z-colors'

export default {
  mixins: [BaseComponent],
  // components: { MbActionsButtons },
  data () {
    return {
      rows: [],
      hidden: [],
      filters: {}
    }
  },
  computed: {
    colors () {
      return colors
    },
    table () {
      return this.$refs.ztable || this.$children[0]
    },
    isGridMode () {
      return this.table !== undefined ? this.table.isGridMode : false
    },
    itemListeners () {
      return {
        'on-delete-action': this.onDelete,
        'on-edit-action': this.onEdit
      }
    },
    _listeners () {
      return {
        'on-delete': this.onDelete,
        'on-edit': (v) => {
          console.log('asasas, no on edit do _listeners do BaseTable', v)
        }
      }
    }
    // _listenersx () {
    //   return {
    //     'on-delete': this.onDelete,
    //     'on-edit': (v) => {
    //       console.log('asasas, no on edit do _listeners do BaseTable', v)
    //     }
    //   }
    // }
  },

  watch: {
    list: function (n, o) {
      this.rows = n
    }
  },
  methods: {
    toggleSubRow (item) {
      this.table.toggleSubRow(item)
    },
    toggleViewMode () {
    },
    filter () {
      this.table.filter()
    },
    onEdit (v) {
      this.$emit('on-edit', v)
    },
    onDelete (v) {
      this.$emit('on-delete', v)
    },
    onRefresh (v) {
      this.$emit('on-refresh', v)
    }
  }
}
