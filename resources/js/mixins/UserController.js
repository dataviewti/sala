import { mapState } from 'vuex'

export default {
  computed: {
    ...mapState({
      activeUser: (state) => state.user.data
    }),
    isAdmin () {
      return this.activeUser?.type_id === 1
    }
  }
}
