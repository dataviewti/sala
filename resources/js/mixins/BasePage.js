import { mapMutations } from 'vuex'

export default {
  data () {
    return {
      dialogs: {},
      tab: null
    }
  },
  computed: {
    formsContainer () {
      return this.$refs.formsContainer || null
    },
    mainForm () {
      return (this.$refs.formsContainer && this.$refs.formsContainer.forms.main) || null
    },
    _tableListeners () {
      return {
        'on-delete': this.onDelete,
        'on-edit': this.onEdit,
        'on-create-new': this.onCreateNew,
        'on-refresh': this.onRefresh
      }
    },
    _formsContainerListeners () {
      return {
        'store-success': this.onStoreSuccess,
        'store-fail': this.onStoreFail,
        'update-success': this.onUpdateSuccess,
        'update-fail': this.onUpdateFail,
        'delete-success': this.onDeleteSuccess,
        'delete-fail': this.onDeleteFail
      }
    },
    _dialogListeners () {
      return {
        'on-open': this.onOpen,
        'close-click': this.onClose,
        'save-click': this.saveClick,
        'update-click': this.updateClick
      }
    }
  },
  created () {
  },
  methods: {
    ...mapMutations({
      overlayShow: 'overlay/show',
      overlayHide: 'overlay/hide',
      overlaySetMessage: 'overlay/setMessage'
    }),
    onRefresh () {
      if (this.module) {
        this.$store
          .dispatch(`${this.module}/load`)
      }
    },
    onDelete (obj) {
      // this.$nextTick(() => {
      // })
      // const url = `${this.form.form.deletePath}/${obj[this.form.primaryKey]}`
      // this.form.delete({
      //   url: url
      // })
    },
    onEdit (obj) {
      // const d = getFirstInstance(this.form, MbDialog, 'dialog')

      // if (d instanceof MbDialog) {
      //   d.open()
      //   this.$nextTick(() => {
      //     this.form.edit(obj)
      //   })
      // }
    },

    onCreateNew () {
      if (this.formsContainer) {
        this.formsContainer.new()
        this.formsContainer.toggleTab()
      }
    },
    onStoreSuccess () {
    },
    onUpdateSuccess () {
    },
    onDeleteSuccess () {
    },
    onStoreFail (e) {
      this.$toast.error(`ocorreram erros, o registro não pode ser salvo: ${e}`)
    },
    onUpdateFail (e) {
      this.$toast.error(`ocorreram erros, o registro não pode ser atualizado: ${e}`)
    },
    onDeleteFail (e) {
      this.$toast.error(`ocorreram erros, o registro não pode ser excluído: ${e}`)
    }
  }
}
