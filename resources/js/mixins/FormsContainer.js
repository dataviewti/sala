import { set } from 'lodash'
import { mapMutations } from 'vuex'
import BaseComponent from '~/mixins/BaseComponent'
import { getFormData } from '~/helpers/z-helpers'

export default {
  mixins: [BaseComponent],
  data () {
    return {
      forms: {},
      tab: null
    }
  },
  computed: {
    mainForm () {
      return this.forms.main
    },
    isEditing () {
      return this.forms.main && this.forms.main.editing !== null
    },
    mainFields () {
      return this.forms.main.fields
    },
    _formsActionsListeners () {
      return {
        'store-click': this.validate,
        'update-click': this.validate,
        'new-click': this.new
      }
    }
  },
  methods: {
    ...mapMutations({
      overlayShow: 'overlay/show',
      overlayHide: 'overlay/hide',
      overlaySetMessage: 'overlay/setMessage'
    }),
    allFields () {
      const map = {}
      for (const form of Object.keys(this.forms)) {
        const _form = this.forms[form]
        for (const field of Object.keys(_form.fields)) {
          const _field = _form.fields[field]
          set(map, _form.autoIndex ? `${form}.${field}` : field, _field)
        }
      }
      return map
    },
    validateAll (p = {}) {
      p = Object.assign({
        formData: false
      }, p)

      let map = {}
      return new Promise((resolve, reject) => {
        try {
          (async () => {
            for (const form of Object.keys(this.forms)) {
              const _form = this.forms[form]
              console.log('no inicio do for', form)
              await _form.validate()
                .then(ret => {
                  if (!_form.autoIndex) {
                    map = Object.assign(map, ret)
                  } else {
                    map[form] = ret
                  }
                })
                .catch(e => {
                  console.log('no catch do validate, deveria abortar aqui')
                  throw e
                })
            }
            if (p.formData) {
              resolve(getFormData(map))
            } else {
              // add o método ao objeto
              map.append = function (i, v) {
                this[i] = v
                delete this.append
              }
              resolve(map)
            }
          })()
          console.log('antes do resolve')
        } catch (err) {
          console.log('catch do try', err)
        }
      })
    },
    onStoreSuccess (data) {
      this.$emit('store-success', data)
    },
    onUpdateSuccess (data) {
      this.$emit('update-success', data)
    },
    deleteSuccess (data) {
      this.$emit('delete-success', data)
    },
    onStoreFail (err) {
      this.$emit('update-fail', err)
    },
    onUpdateFail (err) {
      this.$emit('update-fail', err)
    },
    deleteFail (err) {
      this.$emit('delete-fail', err)
    },
    /** form-actions listeners */
    store (e) {
      this.$emit('store-click', e)
    },
    update (e) {
      this.$emit('update-click', e)
    },
    new (e) {
      for (const form of Object.values(this.forms)) {
        form.reset()
        form.editing = null
      }
      this.$emit('new-click', e)
    }
    // implementar o validate global posteriormente
    // validate (e) {
    //   return new Promise((resolve, reject) => {
    //     (async () => {
    //       // const errors = []
    //       for (const f of Object.values(this.forms)) {
    //         f.validate({ toast: false }).then(ret => {
    //           console.log('RETR', ret)
    //         })
    //           .catch((err) => {
    //             console.log('tem erros', err)
    //           })
    //         // if (!v.valid) { errors.push(el) }
    //       }

    //       // if (errors.length) {
    //       //   console.log('has errors', errors)
    //       //   const v = errors[0].validate({
    //       //     toast,
    //       //     single: true,
    //       //     tabToggle,
    //       //     focus
    //       //   })
    //       //   return reject(new ZException(v.message, v))
    //       // }
    //       return resolve(true)
    //     })()
    //   })
    // }
  }
}
