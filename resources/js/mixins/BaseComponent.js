import { isFunction } from '~/helpers/z-helpers'

export default {
  data () {
    return {
      session: {},
      prefix: '__z_',
      zTab: null,
      zTabEl: null
    }
  },
  props: {
    storageId: {
      type: String,
      default: null
    }
  },
  created () {
    if (this.storageId != null) {
      const comp = localStorage.getItem(`${this.prefix}${this.storageId}`)
      if (comp != null) {
        this.session = JSON.parse(comp)
      } else {
        try {
          localStorage.setItem(`${this.prefix}${this.storageId}`, JSON.stringify({}))
          this.session = {}
        } catch (err) {}
      }
    }
  },
  mounted () {
    this.setRecursiveTabs(this)
  },
  methods: {
    setRecursiveTabs (obj) {
      let el = obj.$parent
      let max = 10
      do {
        if ((el && el.$options._componentTag === 'v-tab-item')) {
          obj.zTab = el.value
          obj.zTabEl = el
          if (isFunction(this.$attrs)) {
            this.$attrs.setProp('zTab', el.value)
            this.$attrs.setProp('zTabEl', el)
          }
          this.setRecursiveTabs(el)
          break
        }

        if (el.$parent === undefined) { break }
        el = el.$parent
        max--
      }
      while (max > 0 && el.zTabEl == null)
    },
    toggleRecursiveTabs (obj = this) {
      if (obj.zTabEl && !obj.zTabEl.isActive) { obj.zTabEl.toggle() }
      if (obj.zTabEl && obj.zTabEl.zTabEl) { this.toggleRecursiveTabs(obj.zTabEl) }
    },
    toggleTab (obj = this) {
      this.toggleRecursiveTabs(this)
    },
    setSession (index, value) {
      globalThis._.set(this.session, index, value)
      localStorage.setItem(`${this.prefix}${this.storageId}`, JSON.stringify(this.session))
    },
    getSession (index, def = null) {
      return globalThis._.get(this.session, index, def)
    }
  }
}
