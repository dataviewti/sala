Storage.prototype.setObject = function (mainKey, key, v) {
  const obj = JSON.parse(this.getItem(mainKey))
  if (arguments.length === 2) this.setItem(mainKey, JSON.stringify(key))
  else {
    Object.defineProperty(obj, key, {
      enumerable: true,
      value: v
    })
    this.setObject(mainKey, obj)
  }
}

Storage.prototype.getObject = function (mainKey, key = null) {
  const obj = JSON.parse(this.getItem(mainKey))
  return key === null ? obj : obj[key]
}

Storage.prototype.removeObject = function (mainKey, key) {
  const obj = JSON.parse(this.getItem(mainKey))
  if (arguments.length === 1) this.removeItem(mainKey)
  else {
    delete obj[key]
    this.setObject(mainKey, obj)
  }
}

// eslint-disable-next-line no-extend-native
Array.prototype.getRandom = function () {
  const yyy = Math.floor(Math.random() * this.length)
  const xxx = this[yyy]
  return xxx
}
