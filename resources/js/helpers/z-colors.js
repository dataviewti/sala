const typeColor = {
  regular: 'gray',
  autoresponder: 'blue',
  automation: 'green'
}

const statusColor = {
  'pending-delete': 'red',
  draft: 'blue',
  sending: 'orange',
  'pending-sending': 'orange',
  'pending-sent': 'orange',
  'pending-processing': 'orange',
  'pending-paused': 'orange',
  'pending-blocked': 'orange'
}

export { statusColor, typeColor }
