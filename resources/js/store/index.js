import Vue from 'vue'
import Vuex from 'vuex'

import user from './modules/user'
import city from './modules/city'
import item from './modules/item'
import category from './modules/category'
import overlay from './modules/overlay'
import config from './modules/config'

Vue.use(Vuex)

export default () => {
  return new Vuex.Store({
    modules: {
      user,
      city,
      item,
      category,
      overlay,
      config
    }
  })
}
