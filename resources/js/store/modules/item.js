import { state, getters, mutations, actions } from './BaseModule'

export default {
  namespaced: true,
  state: () => {
    return {
      ...state,
      loadURL: 'dashboard/items/list'
    }
  },
  getters: {
    ...getters
  },
  mutations: { ...mutations },
  actions: { ...actions }
}
