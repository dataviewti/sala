const state = {
  list: [],
  active: {},
  loadURL: '',
  loadDates: ['created_at', 'updated_at'],
  editing: null,
  update: false
}

const getters = {
  getList: state => state.list
}

const mutations = {
  setList (state, value) {
    state.list = value || []
  },
  set: (context, params = {}) => {
    if (context[params.index] !== undefined) {
      context[params.index] = params.value
    }
  },
  setObj: (context, params = {}) => {
    if (context[params.index] !== undefined) {
      context[params.index] = Object.assign(context[params.index], params.value)
    }
  }
}

const actions = {
  load (context, params = {}) {
    params = Object.assign({
      force: true
    }, params)

    const axiosGet = (url, params) => {
      if (!params.data) {
        return globalThis.axios
          .get(url)
      } else {
        return globalThis.axios
          .post(url, params.data)
      }
    }

    return new Promise((resolve, reject) => {
      try {
        if (!context.updated || params.force) {
          let id = ''
          if (params.id) {
            id = '/' + params.id
          }

          axiosGet(context.state.loadURL + id, params)
            .then(({ data }) => {
              if (data.success) {
                const mappedData = data.data
                context.commit('setList', mappedData)
                resolve({
                  // update: false,
                  data: mappedData
                })
              } else { throw new Error('store load success false') }
            })
            .catch(err => {
              reject(err)
            })
        } else {
          resolve({
            update: true,
            data: context.list
          })
        }
      } catch (err) {
        reject(err)
      }
    })
  }
}

export { state, getters, actions, mutations }
