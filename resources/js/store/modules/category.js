import { state, getters, mutations, actions } from './BaseModule'

export default {
  namespaced: true,
  state: () => {
    return {
      ...state,
      loadURL: 'dashboard/categories/list'
    }
  },
  getters: {
    ...getters,
    listToSelect: (state) => (type) => {
      return state.list.filter(el => {
        return type === undefined
          ? true
          : el.category && el.category.name === type
      })
        .map(el => {
          return {
            text: el.name,
            value: el.id
          }
        })
    }
  },
  mutations: { ...mutations },
  actions: { ...actions }
}
