import { state, getters, mutations, actions } from './BaseModule'

export default {
  namespaced: true,
  state: () => {
    return {
      ...state,
      loadURL: 'dashboard/cities/list'
    }
  },
  getters: {
    ...getters,
    listToSelect (state) {
      const r = state.list.filter(el => {
        return el.status
      })
        .sort((a, b) => a.name - b.name)
        .map(el => {
          return {
            text: el.name,
            value: el.id
          }
        })
      r.unshift({
        text: 'Todos os Municípios',
        value: null
      })

      return r
    }
  },
  mutations: { ...mutations },
  actions: { ...actions }
}
