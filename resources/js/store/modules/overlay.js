export default {
  namespaced: true,
  state: {
    visible: false,
    message: '',
    stayOpen: false
  },
  mutations: {
    show: (context, message) => {
      context.visible = true
      context.message = message || context.message
    },
    hide: context => {
      context.visible = context.stayOpen
    },
    setMessage: (context, value) => {
      context.message = value
      context.visible = true
    },
    setStayOpen: (context, value) => {
      context.stayOpen = value
    }
  }
}
