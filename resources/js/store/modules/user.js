// import { state, getters, mutations, actions } from './BaseModule'

export default {
  namespaced: true,
  state: () => {
    return {
      data: null,
      permissions: []
    }
  },
  getters: {
    cityId: state => {
      return state.data.city_id
    }
  },

  mutations: {
    set (state, n) {
      state.data = n
    },
    setPermissions (state, n) {
      state.permissions = n || []
    }
  },
  actions: {
  }
}
