import { state, getters, mutations, actions } from './BaseModule'

export default {
  namespaced: true,
  state: () => {
    return {
      appUrl: 'localhost'
    }
  },
  getters: {
    ...getters
  },
  mutations: { ...mutations },
  actions: { ...actions }
}
