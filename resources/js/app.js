import Vue from 'vue'
import lodash from 'lodash'
import axios from 'axios'
import store from './store'
// import VueRouter from 'vue-router'
import router from './plugins/router'
import vuetify from './plugins/vuetify'
import VueMask from 'v-mask'
import { Toast, toastOptions } from './plugins/toast' // path to vuetify export
import VueSweetalert2 from 'vue-sweetalert2'
import 'sweetalert2/dist/sweetalert2.min.css'
import colors from './plugins/themes/default'
import './helpers/z-prototypes'
// import './plugins/webfontloader'

require('./bootstrap')

globalThis._ = lodash
globalThis.Vue = Vue
globalThis.axios = axios
// globalThis.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'

Vue.use(Toast, toastOptions)
Vue.use(VueMask)

Vue.use(VueSweetalert2, {
  confirmButtonColor: 'red',
  cancelButtonColor: colors.info
})

// Vue.use(VueRouter)

const files = require.context('./components', true, /\.vue$/i)

files.keys().map(key =>
  Vue.component(
    key
      .split('/')
      .pop()
      .split('.')[0],
    files(key).default
  )
)

Vue.prototype.$eventHub = new Vue()

// eslint-disable-next-line no-unused-vars
const app = new Vue({
  router,
  vuetify,
  store,
  el: '#app'
})
