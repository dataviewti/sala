import ZField from './ZSwitch'
import { url } from '~/helpers/z-validators'

export default class ZDomainField extends ZField {
  constructor (args) {
    super(args)
    this.type = 'url'
    this.default = ''
    this.value = ''
    this.switchButton = true
    this.state = true
    this.lowercase = true
    this.true = {
      icon: 'lock_open',
      color: 'primary',
      prefix: 'http://'
    }

    this.false = {
      icon: 'lock',
      color: 'primary',
      prefix: 'https://'
    }

    this.prefix = 'http://'
    this.rgx = /^(?:(?:https?):\/\/)/

    this.changeState = () => {
      this.state = !this.state
      this.onChangeState(this.state)
    }

    this.onChangeState = (v) => {
      this.prefix = v ? this.true.prefix : this.false.prefix
    }

    this.get = (v) => {
      if (![undefined, null, ''].includes(v)) { return this.rgx.test(v) ? v : `${this.prefix}${v}` }
      return null
    }

    this.set = (v) => {
      const isString = typeof v === 'string'

      if (isString) {
        const _prf = v.match(this.rgx)
        this.value = v.replace(this.rgx, '')

        // atualiza o prefix e state
        if (_prf) {
          if (_prf[0] === this.true.prefix) {
            this.state = true
            this.prefix = this.true.prefix
          } else {
            this.state = false
            this.prefix = this.false.prefix
          }
        }
      } else { this.value = v }

      return this.value
    }

    this.created = (_this) => {
      if (this.required) { this.rules.push(this.emptyRule) }
      this.rules.push(v => {
        return url(['', undefined, null].includes(v) ? null : `${this.prefix}${v.replace(this.rgx, '')}`, this.getEl().__.validator_url)
      })
    }

    Object.assign(this, args)
  }
}
