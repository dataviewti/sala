import ZField from '~/classes/ZField'

export default class ZFile extends ZField {
  constructor (args) {
    super(args)
    this.cType = 'v-file-input'
    this.showSize = true
    this.counter = true
    this.maxSize = false
    this.persistentFile = false
    this.lastFile = null
    this.filled = false
    this.returnObject = true

    this.customGet = () => this.value

    this.customSet = (n) => {
      console.log('tá pegando a custom??', n)
      this.el.model = n?.base64 && n.file.name
        ? this.dataURLtoFile(n.base64, n.file.name)
        : null
    }

    this.getFileInput = () => {
      return this
        .el
        .input
        .$el
        .querySelector("[ctype='v-file-input']")
    }

    this.getUrl = () => {
      return this.value ? URL.createObjectURL(this.value) : undefined
    }

    this.dataURLtoFile = (dataurl, filename) => {
      const arr = dataurl.split(',')
      const mime = arr[0].match(/:(.*?);/)[1]
      const bstr = atob(arr[1]) // msg de deprecated errada, é coisa do node
      let n = bstr.length

      const u8arr = new Uint8Array(n)

      while (n--) {
        u8arr[n] = bstr.charCodeAt(n)
      }

      return new File([u8arr], filename, { type: mime })
    }

    this.mounted = (t) => {
      this.el = t
      if (this.required) {
        this.rules.push((v) => {
          return ![null, undefined].includes(v) ? true : `${this.label} obrigatório`.toLowerCase()
        })
      }

      // if (this.maxSize !== false) {
      //   this.rules.push(v => !v || v.size < 2000000 || 'Avatar size should be less than 2 MB!')
      // }
    }

    this.observer = (t, v) => {
      this.__observer(t, v)
    }

    this.__observer = (t, v) => {
      this.isFilled = ![null, undefined].includes(v)
      if (this.persistentFile) {
        if (v !== undefined) {
          this.lastImage = v
        } else {
          t.$nextTick(() => {
            this.set(this.lastImage)
          })
        }
      }
    }

    Object.assign(this, args)
  }
}
