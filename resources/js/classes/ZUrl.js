import ZField from '~/classes/ZField'
import ZSwitchBtn from '~/classes/ZSwitchBtn'
import { url } from '~/helpers/z-validators'

export default class ZUrl extends ZField {
  constructor (args) {
    super(args)

    this.label = 'Url'
    this.type = 'url'
    this.rules = [
      (v) => {
        return url(v, `${this.label} inválida`.toLowerCase())
      }
    ]

    this.customGet = () => {
      if (this.switch) {
        return this.value
          ? {
              value: this.value,
              target: this.switch.get()
            }
          : null
      }
      return this.get()
    }
    this.set = (n) => {
      this.el.model = null
      if (n && typeof n === 'object') {
        this.el.model = n.value
        this.switch.value = n.target
      } else {
        this.el.model = n
      }
    }

    if (this.target === true || typeof this.target === 'object') {
      this.switch = new ZSwitchBtn(Object.assign({}, {
        slot: 'append-outer',
        true: {
          icon: 'z-whatsapp',
          color: '#3ebd4e',
          label: 'mesma janela'
        },
        false: {
          icon: 'z-whatsapp',
          color: '#ccc',
          label: 'nova janela'
        }
      }, this.target))
    }

    Object.assign(this, args)
  }
}
