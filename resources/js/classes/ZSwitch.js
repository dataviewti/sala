import ZField from './ZField'
export default class ZSwitch extends ZField {
  constructor (args) {
    super(args)
    this.true = true
    this.false = false
    this.value = false
    this.reverse = false
    this.boolean = false
    this.get = (v) => {
      return this.boolean ? this.value : this[this.value]
      // return [this.true, this.false].includes(v) ? v : this.value
    }

    this.set = (v) => {
      // this.value = this.reverse ? !v : Boolean(v)
      this.value = Boolean(v)
    }

    this.created = (_this) => {
      // precisa de correção
      if ((this.required || this.forceRequired === true)) {
        this.rules.push(this.emptyRule)
      }
      if (this.reverse) {
        this.trueValue = false
        this.falseValue = true
      }
    }

    Object.assign(this, args)
  }
}
