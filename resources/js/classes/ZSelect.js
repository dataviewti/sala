import ZField from '~/classes/ZField'

export default class ZSelect extends ZField {
  constructor (args) {
    super(args)
    this.returnObject = true
    this.returnItemValue = false
    this.cType = 'v-combobox'
    this.items = []
    this.itemText = 'text'
    this.itemValue = 'value'
    this.noDataText = 'Nenhum dado'
    this.customIndex = 'id'
    this.method = 'list'

    this.set = (n, data = {}) => {
      this.el.model = this.items
        .find(el => el[this.itemValue] === n) || n
    }

    this.customSet = (n, data = {}) => {
      if (this.multiple === true) {
        const indexes = n.map(el => el[this.customIndex]) || []
        const x = this.items
          .filter(el => indexes.includes(el[this.itemValue]))
        this.el.model = x
      } else {
        this.set(n, data)
      }
    }

    this.customGet = () => {
      return this.el.model
        ? this.returnObject && this.returnItemValue
          ? this.multiple === true
            ? this.el.model.map(el => el[this.itemValue])
            : this.el.model.value
          : this.el.model
        : null
    }

    Object.assign(this, args)
  }
}
