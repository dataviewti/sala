export default class ZSwitchBtn {
  constructor (args) {
    this.value = true
    this.slot = 'prepend-inner'
    this.true = {
      icon: 'lock_open',
      color: 'red',
      label: 'aasasasa'
    }

    this.false = {
      icon: 'lock',
      color: 'primary',
      label: 'trueasdasdasd'
    }

    this.toggle = () => {
      this.value = !this.value
    }

    this.get = (v) => {
      return this.value
    }

    this.set = (v) => {
      this.state = v
    }

    Object.assign(this, args)
  }
}
