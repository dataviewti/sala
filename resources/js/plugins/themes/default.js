// default MB theme colors;

export default {
  primary: '#94B4E4',
  secondary: '#E6E6E6',
  accent: '#82B1FF',
  error: '#FF8080',
  info: '#2196F3',
  success: '#F3B888',
  warning: '#FFFF80',
  azul: '#1ea5c3',
  verde: '#00B878',
  laranja: '#FF7537'
}
