import Vue from 'vue'
import 'vuetify/dist/vuetify.min.css'
import Vuetify from 'vuetify'

// import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader
// import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader
import light from './themes/default'
import ZIcons from './z-icons'
import router from './router'

Vue.use(Vuetify)

export default new Vuetify({
  router,
  icons: { iconfont: 'md', values: ZIcons },
  theme: {
    themes: {
      light
    }
  }
})
