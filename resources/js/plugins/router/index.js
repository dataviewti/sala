import Vue from 'vue'
import Router from 'vue-router'

import LinkPage from '~services/links/LinkPage.vue'
import ConfigPage from '~services/config/config-page.vue'
import DashPage from '~services/dash/DashPage.vue'
import DocPage from '~services/docs/DocPage.vue'
import CityPage from '~services/cities/CityPage.vue'
// import goTo from 'vuetify/es5/services/goto'

Vue.use(Router)
const router = new Router({
  routes: [
    { path: '/cities', name: 'cities', component: CityPage },
    { path: '/cities', name: 'cities', component: CityPage },
    { path: '/links', name: 'links', component: LinkPage },
    { path: '/config', name: 'config', component: ConfigPage },
    { path: '/docs', name: 'docs', component: DocPage },
    // { path: '/dash', name: 'dash', component: () => DashPage },
    { path: '/dash', name: 'dash', component: DashPage },
    { path: '*', redirect: '/dash' }
  ]
})

export default router
