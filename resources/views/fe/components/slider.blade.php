@php
    use Illuminate\Support\Facades\Storage;
    
    if(filled($city->file)){
        $img = 'storage/'.$city->file->file_path;
    }
    else{
        $img = '/images/defaults/city-default.jpg';
    }
@endphp
<div id="home" class="owl-carousel owl-carousel-light owl-carousel-light-init-fadeIn owl-theme manual dots-inside dots-horizontal-center show-dots-hover nav-inside nav-inside-plus nav-dark nav-md nav-font-size-md show-nav-hover mb-0" data-plugin-options="{'autoplay': false}" style="height: 600px;">
	<div class="owl-stage-outer">
		<div class="owl-stage">
			<div class="owl-item position-relative overlay overlay-show overlay-op-5 pt-5" style="background-image: url({{$img}}); background-size: cover; background-position: center;">
				<div class="container position-relative z-index-3 h-100">
					<div class="row justify-content-center align-items-center h-100">
						<div class="col-lg-6">
							<div class="d-flex flex-column align-items-center">
								<h3 class="position-relative text-color-light text-5 line-height-5 font-weight-medium px-4 mb-2 appear-animation" data-appear-animation="fadeInDownShorterPlus" data-plugin-options="{'minWindowWidth': 0}">
									<span class="position-absolute right-100pct top-50pct transform3dy-n50 opacity-3">
										<img src="img/slides/slide-title-border.png" class="w-auto appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="250" data-plugin-options="{'minWindowWidth': 0}" alt="" />
									</span>
									Sala do Empreendedor Virtual
									<span class="position-absolute left-100pct top-50pct transform3dy-n50 opacity-3">
										<img src="img/slides/slide-title-border.png" class="w-auto appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="250" data-plugin-options="{'minWindowWidth': 0}" alt="" />
									</span>
								</h3>
                                <h2 style="letter-spacing:2px; line-height:80px" class="text-center text-color-light font-weight-extra-bold display-1 mb-3 mt-3 appear-animation" data-appear-animation="blurIn" data-appear-animation-delay="500" data-plugin-options="{'minWindowWidth': 0}">{{Str::upper($cityName)}}</h2>
								<!-- <p class="text-4 text-color-light font-weight-light opacity-7 text-center mb-0" data-plugin-animated-letters data-plugin-options="{'startDelay': 1000, 'minWindowWidth': 0, 'animationSpeed': 25}">sala do empreendedor virtual</p> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>