@php
	$mainUrl = config('app.url');
@endphp

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>Sala do Empreendedor Digital</title>	

		<meta name="keywords" content="saladoempreendedor, sala do empreendedor virtual, sebrae, sebrae Tocantins, tocantins, empreendedorismo, mei, micro empreendedor individual" />
		<meta name="description" content="Portal da sala do empreendedor virtual dos municípios do Tocantins">
		<meta name="author" content="Sebrae Tocantins">

		@include('base.layout.favicon')

		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
		<!-- Google font -->
		<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Lato:400" rel="stylesheet">

		<!-- Font Awesome Icon -->
		<!-- <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/fontawesome.min.css" /> -->

		<!-- Custom stlylesheet -->
		<link type="text/css" rel="stylesheet" href="css/404.css" />

	</head>
	<body>
		<div id="notfound">
			<div class="notfound-bg">
				<div></div>
				<div></div>
				<div></div>
				<div></div>
			</div>
			<div class="notfound">
				<div class="notfound-404">
					<h1>404</h1>
				</div>
				<h2>Página não encontrada</h2>
				<p>A página que você está procurando pode ter sido removida devido a mudança de nome ou está temporariamente indisponível.</p>
				<a href="{{$mainUrl}}">Página principal</a>
			</div>
		</div>
	</body>
</html>
