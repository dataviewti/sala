@php
	use Illuminate\Database\Eloquent\Builder;
	use App\Models\City;
	use App\Models\Item;

    $cat = "MEI";

	$merged = Item::select()
		->whereHas('categories', function (Builder $query) use ($cat){
    		$query
				->where('name',$cat);
		})
		->where('city_id',$city->id)
        ->orWhereNull('city_id')
		->whereHas('categories', function (Builder $query) use ($cat){
    		$query
				->where('name',$cat);
		})
		->orderBy('order')
		->get();

		
@endphp

<div class="heading heading-border heading-middle-border heading-middle-border-center my-5 mb-2">
	<h1 class="font-weight-normal"><strong class="font-weight-extra-bold">Micro Empreendedor Individual - {{$cat}}</strong></h1>
</div>
<h4 class="text-center text-primary">tudo para facilitar a vida do MEI em um só lugar</h4>

<div class="container pt-2 featured-boxes featured-boxes-style-3 featured-boxes-flat">
	<div class="row justify-content-center">
		@foreach($merged as $i)
			<div class="{{$data->cols}} justify-content-center">
				<x-fe.item :item="$i"/>
			</div>
		@endforeach
	</div>
</div>

