@php
	use Illuminate\Database\Eloquent\Builder;
	use App\Models\City;
	use App\Models\Item;

	$local = Item::select()
		->whereHas('mainCategory', function (Builder $query) {
    		$query
				->where('name','Serviços Estaduais');
		})
		->orderBy('order')
		->get();

	$global= Item::select()
		->whereHas('mainCategory', function (Builder $query) {
    		$query
				->where('name','Serviços Estaduais');
		})
		->whereNull('city_id')
		->orderBy('order')
		->get();

	$g = $global;
		
@endphp

<div class="heading heading-border heading-middle-border heading-middle-border-center my-5">
	<h1 class="font-weight-normal"><strong class="font-weight-extra-bold">Serviços Estaduais</strong></h1>
</div>

<div class="container pt-2 featured-boxes featured-boxes-style-3 featured-boxes-flat">
	<!-- <hr /> -->
	<div class="row justify-content-center">
		@foreach($global as $i)
			<div class="{{$data->cols}} justify-content-center">
				<x-fe.item type="featured-box-tertiary" :item="$i"/>
			</div>
		@endforeach
	</div>
</div>

