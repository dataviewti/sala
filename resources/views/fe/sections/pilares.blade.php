<section id="services" class="section section-height-3 section bg-color-grey-scale-1 border-0 m-0
 border-0 m-0 appear-animation" data-appear-animation="fadeIn">
			    <div class="container pt-5 pb-3">
			        <div class="row">
			            <div class="col text-center">
			                <h3 class="font-weight-bold text-color-default line-height-1 text-4 ls-0 mb-1">sala do empreendedor</h3>
			                <h2 class="text-color-dark font-weight-bold text-8">Conheça os Pilares da Sala do Empreendedor</h2>
			            </div>
			        </div>


					<div class="row process custom-process justify-content-center mt-4">
					<div class="process-step col-md-9 col-lg-6 mb-5 mb-lg-4 appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200" style="animation-delay: 200ms;">
						<div class="process-step-circle border-width-3 mb-3">
			                    <strong class="process-step-circle-content">
			                        <img width="58" height="58" src="img/demos/finance/icons/box.svg" alt="" data-icon="" data-plugin-options="{'onlySVG': true, 'extraClass': ''}" class="svg-stroke-color-primary" style="opacity: .5; width: 58px;">
			                    </strong>
			            </div>
						<div class="process-step-content px-4">
							<h4 class="font-weight-bold text-6 pb-1 mb-2">Formalização</h4>
							<p class="text-3-5 text-color-dark mb-0">
								Na Sala do Empreendedor Virtual você também poderá formalizar seu negócio através de um atendimento virtual. Basta reunir a documentação exigida clique aqui e solicitar o atendimento do Agente de Desenvolvimento.
							</p>
						</div>
					</div>
			            <div class="process-step col-md-9 col-lg-6 mb-5 mb-lg-4 appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400" style="animation-delay: 400ms;">
			                <div class="process-step-circle border-width-3 mb-3">
			                    <strong class="process-step-circle-content">
			                        <img width="58" height="58" src="img/demos/finance/icons/refresh-cw.svg" alt="" data-icon="" data-plugin-options="{'onlySVG': true, 'extraClass': ''}" class="svg-stroke-color-primary" style="opacity: 1; width: 58px;">
			                    </strong>
			                </div>
			                <div class="process-step-content px-4">
			                    <h4 class="font-weight-bold text-6 pb-1 mb-2">Simplificação</h4>
			                    <p class="text-3-5 text-color-dark mb-0">
									Disponibilização de diversos serviços de forma eletrônica, através da Sala do Empreendedor Virtual, otimizando o atendimento ao empreendedor, além de unicidade de processos e a concentração em um mesmo espaço de diversos serviços próprios do município e de parceiros. Consulte o seu município.
								</p>
			                </div>
			            </div>
				</div>



				<div class="row process custom-process justify-content-center mt-4">
					<div class="process-step col-md-9 col-lg-6 mb-5 mb-lg-4 appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="600" style="animation-delay: 600ms;">
			                <div class="process-step-circle border-width-3 mb-3">
			                    <strong class="process-step-circle-content">
			                        <img width="58" height="58" src="img/demos/finance/icons/people.svg" alt="" data-icon="" data-plugin-options="{'onlySVG': true, 'extraClass': ''}" class="svg-fill-color-primary" style="opacity: 1; width: 58px;">
			                    </strong>
			                </div>
			                <div class="process-step-content px-4">
			                    <h4 class="font-weight-bold text-6 pb-1 mb-2">Capacitação</h4>
			                    <p class="text-3-5 text-color-dark mb-0">
									Através do menu Agenda, o empreendedor tem acesso a informações relacionadas a eventos e capacitações locais com parcerias entre Sebrae e Município, como foco no desenvolvimento local, autogestão e aprimoramento do negócio.
								</p>
							</div>
			            </div>
						<div class="process-step col-md-9 col-lg-6 mb-5 mb-lg-4 appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200" style="animation-delay: 200ms;">
			                <div class="process-step-circle border-width-3 mb-3">
			                    <strong class="process-step-circle-content">
			                        <img width="58" height="58" src="img/demos/finance/icons/map-pin.svg" alt="" data-icon="" data-plugin-options="{'onlySVG': true, 'extraClass': ''}" class="svg-stroke-color-primary" style="opacity: .5; width: 58px;">
			                    </strong>
			                </div>
			                <div class="process-step-content px-4">
			                    <h4 class="font-weight-bold text-6 pb-1 mb-2">Oportunidades de Compras Públicas</h4>
			                    <p class="text-3-5 text-color-dark mb-0">
									Tenha acesso a informação sobre estimativa de compras (bens de consumo) e editais de licitação que estão em vigência no seu município. Tire suas dúvidas sobre a documentação exigida para participação.                                
								</p>
			                </div>
			            </div>
			        </div>
			    </div>
			</section>




