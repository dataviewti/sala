@php
	use Illuminate\Database\Eloquent\Builder;
	use App\Models\City;
	use App\Models\Item;

	$cat = 'Tutoriais Passo a Passo';

	$local = Item::select()
		->whereHas('mainCategory', function (Builder $query) use($cat) {
    		$query
				->where('name',$cat);
		})
		->where('city_id',$city->id)
		->orderBy('order')
		->get();

	$global= Item::select()
		->whereHas('mainCategory', function (Builder $query) use ($cat) {
    		$query
				->where('name',$cat);
		})
		->whereNull('city_id')
		->orderBy('order')
		->get();

		$has = count(collect($global,$local)->all())>0;
		
@endphp

<div class="heading heading-border heading-middle-border heading-middle-border-center my-5 mb-2">
	<h1 class="font-weight-normal"><strong class="font-weight-extra-bold">{{$cat}}</strong></h1>
</div>

@if($has)

	<div class="container pt-2 featured-boxes featured-boxes-style-3 featured-boxes-flat">
		@if(count($local)>1)
			<div class="row justify-content-center">
				@foreach($local as $i)
					<div class="{{$data->cols}} justify-content-center">
						<x-fe.item :item="$i"/>
					</div>
				@endforeach
			</div>
			<hr />
		@endif
		<div class="row justify-content-center">
			@foreach($global as $i)
				<div class="{{$data->cols}} justify-content-center">
					<x-fe.item type="featured-box-tertiary" :item="$i"/>
				</div>
			@endforeach
		</div>
	</div>
@else
	@include("fe.components.no-item");
@endif


