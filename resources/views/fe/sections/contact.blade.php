@php
    use App\Traits\FE;

    $phone = FE::getPhone($city->phone);
    $mobile = filled($city->mobile) ? FE::getPhone($city->mobile) : $phone;

@endphp
<div class="heading heading-border heading-middle-border heading-middle-border-center my-5">
	<h1 class="font-weight-normal"><strong class="font-weight-extra-bold">Contato</strong></h1>
</div>

<div role="main" class="main">

<!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
<!-- <div id="googlemaps" class="google-map mt-0 b-red" style="height: 500px;"></div> -->
@if(filled($city->google_maps))
	<div class ="row d-flex">
		<div class="col-sm-12 mx-auto">
			{!!$city->google_maps!!}
		</div>
	</div>
@endif

<div class="container">

	<div class="row py-4">
		<div class="col-lg-6">

			<h2 class="font-weight-bold text-8 mt-2 mb-0" id="fale-conosco">Fale Conosco</h2>
			<p class="mb-4">Dúvidas, sugestões, reclamações, entre em contato que responderemos o mais breve possível</p>

			<form class="contact-form" action="php/contact-form.php" method="POST">
				<div class="contact-form-success alert alert-success d-none mt-4">
					<strong>Successo!</strong> Sua mensagem foi enviada com sucesso!
				</div>

				<div class="contact-form-error alert alert-danger d-none mt-4">
					<strong>Erro!</strong> Ocorreram problemas no envio de sua mensagem!.
					<span class="mail-error-message text-1 d-block"></span>
				</div>

				<div class="row">
					<div class="form-group col-lg-6">
						<label class="form-label mb-1 text-2">Nome Completo</label>
						<input type="text" value="" 
						data-msg-required="Please enter your name."
						 maxlength="100" class="form-control text-3 h-auto py-2"
						 name="name" required>
					</div>
					<div class="form-group col-lg-6">
						<label class="form-label mb-1 text-2">Email</label>
						<input type="email" value="" 
						data-msg-required="Please enter your email address."
						data-msg-email="Please enter a valid email address."
						maxlength="100" class="form-control text-3 h-auto py-2"
						name="email" required>
					</div>
				</div>
				<div class="row">
					<div class="form-group col">
						<label class="form-label mb-1 text-2">Assunto</label>
						<input type="text" value=""
						data-msg-required="Please enter the subject."
						maxlength="100" class="form-control text-3 h-auto py-2"
						name="subject" required>
					</div>
				</div>
				<div class="row">
					<div class="form-group col">
						<label class="form-label mb-1 text-2">Mensagem</label>
						<textarea maxlength="5000" 
						data-msg-required="Please enter your message."
						rows="8" class="form-control text-3 h-auto py-2"
						name="message" required></textarea>
					</div>
				</div>
				<div class="row">
					<div class="form-group col">
						<input type="submit" value="Enviar Mensagem" class="btn btn-primary btn-modern" data-loading-text="enviando...">
					</div>
				</div>
			</form>

		</div>
		<div class="col-lg-6">

			<div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="800">
				<h4 class="mt-2 mb-1">Sala do <strong>Empreendedor</strong></h4>
				<ul class="list list-icons list-icons-style-2 mt-2">
					<li><i class="fas fa-map-marker-alt top-6"></i> 
						<strong class="text-dark">Endereço:</strong><br />
						 {!! $city->fullAddress() !!}
					</li>
					@if(filled($mobile->value))
						<li>
							@if(optional($phone)->whatsapp)
								<i class="fab fa-whatsapp top-6"></i>
								<strong class="text-dark">Whatsapp:</strong>
								<a target="_blank" href="{{$phone->url}}">
									{{$phone->value}}
								</a>
							@else
								<i class="fas fa-phone top-6"></i>
								<strong class="text-dark">Telefone:</strong>
								<a target="_blank" href="tel:{{optional($phone)->cleanValue}}">
									{{$phone->value}}
								</a>
							@endif
						</li>
					@endif
					@if(filled($mobile->value))
						<li>
							@if(optional($mobile)->whatsapp)
								<i class="fab fa-whatsapp top-6"></i>
								<strong class="text-dark">Whatsapp:</strong>
								<a target="_blank" href="{{$mobile->url}}">
									{{$mobile->value}}
								</a>
							@else
								<i class="fas fa-phone top-6"></i>
								<strong class="text-dark">Telefone:</strong>
								<a target="_blank" href="tel:{{$mobile->cleanValue}}">
									{{$mobile->value}}
								</a>
							@endif
						</li>
					@endif
					@if(filled($city->email))
						<li><i class="fas fa-envelope top-6"></i> <strong class="text-dark">Email:</strong> <a href="mailto:{{$city->email}}">{{$city->email}}</a></li>
					@endif
				</ul>
			</div>

			<div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="950">
				<h4 class="pt-5">Horário de <strong>Atendimento</strong></h4>
				<ul class="list list-icons list-dark mt-2">
					<li><i class="far fa-clock top-6"></i> {{$city->office_hours}}</li>
				</ul>
			</div>

			<!-- <h4 class="pt-5">Get in <strong>Touch</strong></h4>
			<p class="lead mb-0 text-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->

		</div>

	</div>

</div>

</div>