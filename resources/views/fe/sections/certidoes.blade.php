@php
	use Illuminate\Database\Eloquent\Builder;
	use App\Models\City;
	use App\Models\Item;

    $cat = "Certidões Negativas";

	$merged = Item::select()
		->whereHas('categories', function (Builder $query) use ($cat){
    		$query
				->where('name',$cat);
		})
		->where('city_id',$city->id)
        ->orWhereNull('city_id')
		->whereHas('categories', function (Builder $query) use ($cat){
    		$query
				->where('name',$cat);
		})
		->orderBy('order')
		->get();

		$has = count(collect($merged)->all())>0;

@endphp

<div class="heading heading-border heading-middle-border heading-middle-border-center my-5">
	<h1 class="font-weight-normal"><strong class="font-weight-extra-bold">{{$cat}}</strong></h1>
</div>
@if($has)
	<div class="container pt-2 featured-boxes featured-boxes-style-3 featured-boxes-flat">
		<div class="row justify-content-center">
			@foreach($merged as $i)
				<div class="{{$data->cols}} justify-content-center">
					<x-fe.item :item="$i"/>
				</div>
			@endforeach
		</div>
	</div>
@else
	@include("fe.components.no-item");
@endif

