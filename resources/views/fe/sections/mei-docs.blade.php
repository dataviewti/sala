@php
	use Illuminate\Database\Eloquent\Builder;
	use App\Models\City;
	use App\Models\Item;

    $cat = "MEI";

	$merged = Item::select()
		->whereHas('categories', function (Builder $query) use ($cat){
    		$query
				->where('name',$cat);
		})
		->where('city_id',$city->id)
        ->orWhereNull('city_id')
		->whereHas('categories', function (Builder $query) use ($cat){
    		$query
				->where('name',$cat);
		})
		->orderBy('order')
		->get();

		
@endphp

<div class="heading heading-border heading-middle-border heading-middle-border-center my-5 mb-2">
	<h1 class="font-weight-normal"><strong class="font-weight-extra-bold">Micro Empreendedor Individual - {{$cat}}</strong></h1>
</div>
<h4 class="text-center text-primary">-</h4>

<div class="row justify-content-center">
    <div class="col-10 justify-content-center">
        <div class="nav nav-tabs nav-pills" id="nav-tab" role="tablist">
            <button class="nav-link active" id="nav-entenda-tab" data-bs-toggle="tab" data-bs-target="#nav-entenda" type="button" role="tab" aria-controls="nav-entenda" aria-selected="true">Entenda o que é o MEI</button>
            <button class="nav-link"        id="nav-formalizar-tab" data-bs-toggle="tab" data-bs-target="#nav-formalizar" type="button" role="tab" aria-controls="nav-formalizar" aria-selected="false">Como se formalizar</button>
            <button class="nav-link"        id="nav-ocupacoes-tab" data-bs-toggle="tab" data-bs-target="#nav-ocupacoes" type="button" role="tab" aria-controls="nav-ocupacoes" aria-selected="false">Ocupações</button>
            <button class="nav-link"        id="nav-duvidas-tab" data-bs-toggle="tab" data-bs-target="#nav-duvidas" type="button" role="tab" aria-controls="nav-duvidas" aria-selected="false">Principais dúvidas</button>
            <button class="nav-link"        id="nav-empregado-tab" data-bs-toggle="tab" data-bs-target="#nav-empregado" type="button" role="tab" aria-controls="nav-empregado" aria-selected="false">MEI que contrata empregado</button>
            <button class="nav-link"        id="nav-municipio-tab" data-bs-toggle="tab" data-bs-target="#nav-municipio" type="button" role="tab" aria-controls="nav-municipio" aria-selected="false">Obrigações do MEI com o município</button>
        </div>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-entenda" role="tabpanel" aria-labelledby="nav-entenda-tab">
                @include('fe.sections.mei.entenda')
            </div>
            <div class="tab-pane fade" id="nav-formalizar" role="tabpanel" aria-labelledby="nav-formalizar-tab">
            </div>
            <div class="tab-pane fade" id="nav-ocupacoes" role="tabpanel" aria-labelledby="nav-ocupacoes-tab">...</div>
            <div class="tab-pane fade" id="nav-duvidas" role="tabpanel" aria-labelledby="nav-duvidas-tab">...</div>
            <div class="tab-pane fade" id="nav-empregado" role="tabpanel" aria-labelledby="nav-empregado-tab">...</div>
            <div class="tab-pane fade" id="nav-municipio" role="tabpanel" aria-labelledby="nav-municipio-tab">...</div>
        </div>
    </div>
</div>




