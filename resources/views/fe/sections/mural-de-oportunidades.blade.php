@php
	use Illuminate\Database\Eloquent\Builder;
	use App\Models\City;
	use App\Models\Item;

	$local = Item::select()
		->whereHas('mainCategory', function (Builder $query) {
    		$query
				->where('name','Mural de Oportunidades');
		})
		->where('city_id',$city->id)
		->orderBy('order')
		->get();

	$global= Item::select()
		->whereHas('mainCategory', function (Builder $query) {
    		$query
				->where('name','Mural de Oportunidades');
		})
		->whereNull('city_id')
		->orderBy('order')
		->get();
	
	$has = count(collect($global,$local)->all())>0;

		
@endphp

@if($has)
	<div class="heading heading-border heading-middle-border heading-middle-border-center my-5">
		<h1 class="font-weight-normal"><strong class="font-weight-extra-bold">Mural de Oportunidades</strong></h1>
	</div>

	<div class="container pt-2 featured-boxes featured-boxes-style-3 featured-boxes-flat">
		<div class="row justify-content-center">
			@foreach($local as $i)
				<div class="{{$data->cols}} justify-content-center">
					<x-fe.item :item="$i"/>
				</div>
			@endforeach
		</div>
		<div class="row justify-content-center">
			@foreach($global as $i)
				<div class="{{$data->cols}} justify-content-center">
					<x-fe.item type="featured-box-tertiary" :item="$i"/>
				</div>
			@endforeach
		</div>
	</div>
@endif
