@php
    use Illuminate\Support\Str;
    use App\Traits\FE;
    
    $cityName = $city->name;
    $title = "Sala do Empreendedor ${cityName} - Sebrae/TO";
        
@endphp

@extends('fe.layout.default',[
    "overlay"=>true
])

@section('content')
    @include('fe.components.slider')

    <section id = 'pilares'>
        @include('fe.sections.pilares')
    </section>

    <section id = 'mural-de-oportunidades'>
        @include('fe.sections.mural-de-oportunidades')
    </section>

    <section id = 'servicos'>
        @include('fe.sections.servicos')
    </section>

    <section id = 'servicos-estaduais'>
        @include('fe.sections.servicos-estaduais')
    </section>

    <section id = 'certidoes'>
        @include('fe.sections.certidoes')
    </section>

    <section id = 'mei'>
        @include('fe.sections.mei')
    </section>

    <!-- <section id = 'mei-docs'>
        @include('fe.sections.mei-docs')
    </section> -->
    
    <section id = 'tutoriais'>
        @include('fe.sections.tutoriais')
    </section>

    @if(filled($city->google_schedule))
        <section id = 'agenda'>
            @include('fe.sections.agenda')
        </section>
    @endif

    <section id = 'contato'>
        @include('fe.sections.contact')
    </section>
@endsection

@section('scripts-before-body-close')
    @include('base.components.google-maps')
@endsection


