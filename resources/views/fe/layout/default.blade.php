<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	
		<meta http-equiv="Cache-control" content="public">
		<meta http-equiv="pragma" content="no-cache" />
		<meta name="robots" content="all" />
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
		@yield('metatags')

		<title>{{$title}}</title>	

		@include('base.layout.favicon')

		<link id="googleFonts" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7CShadows+Into+Light&display=swap" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!-- Vendor CSS -->
		<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="vendor/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="vendor/animate/animate.compat.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-elements.css">
		<link id="skinCSS" rel="stylesheet" href="css/skins/default.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="css/custom.css">
		<link rel="stylesheet" href="css/z-fe.css">

	</head>
	@if($overlay)
		<body data-plugin-page-transition 
			data-plugin-scroll-spy
			class="loading-overlay-showing" 
			data-loading-overlay 
			data-plugin-options="{'hideDelay': 500, 'effect': 'speedingWheel'}">
		@include('fe.components.overlay')
	@else
		<body  data-plugin-scroll-spy data-plugin-scroll-spy>
	@endif
	<div class="body">
		@include('fe.layout.header')
		<div role="main" class="main">
			@yield('content')
		</div>

		<section id = 'partners'>
    	    @include('fe.sala.partners')
	    </section>
		
		<footer id="footer">
			@include('fe.layout.footer')
		</footer>
	</div>

		<!-- Vendor -->
		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="vendor/jquery.cookie/jquery.cookie.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="vendor/jquery.validation/jquery.validate.min.js"></script>
		<!-- <script src="vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script> -->
		<!-- <script src="vendor/jquery.gmap/jquery.gmap.min.js"></script> -->
		<!-- <script src="vendor/lazysizes/lazysizes.min.js"></script> -->
		<!-- <script src="vendor/isotope/jquery.isotope.min.js"></script> -->
		<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
		<!-- <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script> -->
		<!-- <script src="vendor/vide/jquery.vide.min.js"></script> -->
		<!-- <script src="vendor/vivus/vivus.min.js"></script> -->


		<!-- Theme Base, Components and Settings -->
		<script src="js/theme.js"></script>

		<!-- Theme Custom -->
		<script src="js/custom.js"></script>

		<!-- Theme Initialization Files -->
		<script src="js/theme.init.js"></script>

		@yield('scripts-before-body-close')
	</body>
</html>
