@php
    use App\Traits\FE;

    $phone = FE::getPhone($city->phone);
    $mobile = filled($city->mobile) ? FE::getPhone($city->mobile) : $phone;
    $mainUrl = config('app.url')."/".$city->getSlug();

@endphp

<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyStartAt': 108, 'stickySetTop': '-109px', 'stickyChangeLogo': true}">
    <div class="header-body d-none d-sm-block">
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-logo">
                            <a href="{{$mainUrl}}">
                                <img alt="Logo Sala do Empreendedor" width="auto" height="165" data-sticky-width="auto" data-sticky-height="82" data-sticky-top="94" src="images/front/logo-sala-do-empreendedor.png">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="header-column justify-content-end">
                    <div class="header-row pt-3">
                        <nav class="header-nav-top">
                            <ul class="nav nav-pills">
                                <li class="nav-item nav-item-anim-icon d-none d-md-block">
                                    <a class="nav-link ps-0" href="#pilares"  data-hash data-hash-offset="68"><i class="fas fa-angle-right"></i> O Projeto</a>
                                </li>
                                <li class="nav-item nav-item-anim-icon d-none d-md-block">
                                    <a class="nav-link" href="#fale-conosco" data-hash data-hash-offset="150"><i class="fas fa-angle-right"></i> Fale Conosco</a>
                                </li>
                                @if(filled($mobile->value))
                                    <li class="nav-item nav-item-left-border">
                                        @if(optional($phone)->whatsapp)
                                            <a class="nav-link" target="_blank" href="{{$phone->url}}">
                                                <i class="fas z-whatsapp"></i>
                                                {{$phone->value}}
                                            </a>
                                        @else
                                            <a class="nav-link" target="_blank" href="tel:{{optional($phone)->cleanValue}}">
                                                <i class="fas z-phone"></i>{{$phone->value}}
                                            </a>
                                        @endif
                                    </li>
                                @endif
                                @if(filled($city->email))
                                    <li class="nav-item nav-item-left-border">
                                        <a class="nav-link" href="#">
                                            <i class="fab fab-email"></i>
                                             {{$city->email}}
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </nav>
                        <!-- <div class="header-nav-features">
                            <div class="header-nav-feature header-nav-features-search d-inline-flex">
                                <a href="#" class="header-nav-features-toggle text-decoration-none" data-focus="headerSearch"><i class="fas fa-search header-nav-top-icon"></i></a>
                                <div class="header-nav-features-dropdown" id="headerTopSearchDropdown">
                                    <form role="search" action="page-search-results.html" method="get">
                                        <div class="simple-search input-group">
                                            <input class="form-control text-1" id="headerSearch" name="q" type="search" value="" placeholder="procurar por...">
                                            <button class="btn" type="submit">
                                                <i class="fas fa-search header-nav-top-icon"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <div class="header-row">
                        <ul class="header-extra-info d-flex align-items-center">
                            <li class="d-none d-md-inline-flex">
                                <div class="header-extra-info-text d-flex">
                                    <div>
                                        <i class="header-icon icon-hours z-clock"></i>
                                    </div>
                                    <div>
                                        <label>HORÁRIO DE FUNCIONAMENTO</label>
                                        <strong>{{$city->office_hours}}</strong>
                                    </div>
                                </div>
                            </li>
                            <!-- <li class="d-none d-sm-inline-flex">
                                <div class="header-extra-info-text">
                                    <label>ENVIE UM EMAIL</label>
                                    <strong><a href="mailto:mail@example.com">CONTATO@TESTE.COM.BR</a></strong>
                                </div>
                            </li> -->
                            @if(filled($mobile->value))
                                <li>
                                    <div class="header-extra-info-text d-flex">
                                        @if($mobile->whatsapp)
                                            <div>
                                                <i class="header-icon z-whatsapp icon-whatsapp"></i>
                                            </div>
                                            <div>
                                                <label>ATENDIMENTO VIA WHATSAPP</label>
                                                <strong>
                                                    <a target="_blank" href="{{$mobile->url}}">
                                                        {{$mobile->value}}
                                                    </a>
                                                </strong>
                                            </div>
                                        @else
                                            <div>
                                                <i class="header-icon z-phone icon-hours"></i>
                                            </div>
                                            <div>
                                                <label>ATENDIMENTO VIA TELEFONE</label>
                                                <strong>
                                                    <a target="_blank" href="tel:{{$mobile->cleanValue}}">
                                                        {{$mobile->value}}
                                                    </a>
                                                </strong>
                                            </div>
                                        @endif
                                    </div>
                                </li>
                            @endif
                        </ul>
                    </div>
                    <div class="header-row">
                        @include('fe.layout.header.menu')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="header-body d-block d-sm-none">
        <div class="header-container container" style="background-color:#fff!important">

            <div class="header-row">
                <div class="header-logo header-logo-sticky-change d-none d-sm-block" style="width: auto; height: 110px;">
                    <a href="{{config('url')}}">
                        <img class="header-logo-non-sticky opacity-0" alt="Sala do Empreendedor" width="auto" height="110" src="images/front/logo-sala-do-empreendedor.png">
                        <img class="header-logo-sticky opacity-0" alt="Sala do Empreendedor" width="auto" height="60" style="margin-top:25px!important; max-height:60px!important" src="images/front/logo-sala-do-empreendedor-vert.png">
                    </a>
                </div>
                <div class="header-logo header-logo-sticky-change d-block d-sm-none" style="width: auto; height: 110px;">
                    <a href="{{config('url')}}">
                        <img class="header-logo-non-sticky" alt="Sala do Empreendedor" width="auto" height="110" src="images/front/logo-sala-do-empreendedor.png">
                    </a>
                </div>
            </div>

            <div class="header-column justify-content-end">
                <div class="header-row">
                    <div class="header-nav header-nav-links header-nav-dropdowns-dark header-nav-light-text order-2 order-lg-1">
                        <div class="header-nav-main header-nav-main-effect-2 header-nav-main-font-lg header-nav-main-font-lg-upper-2 header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
                            <nav class="collapse">
                                <ul class="nav nav-pills" id="mainNav">
                                    <li>
                                        <a data-hash class="nav-link active" href="{{$mainUrl}}"> 
                                            <i class="fas fa-home"></i> &nbsp;
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#pilares"  data-hash data-hash-offset="68"> 
                                            O Projeto
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#servicos"  data-hash data-hash-offset="100">
                                            Serviços
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#certidoes" data-hash data-hash-offset="100">
                                            Certidões
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#mei" data-hash data-hash-offset="100">
                                            MEI
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tutoriais" data-hash data-hash-offset="100">
                                            Tutoriais
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#agenda" data-hash data-hash-offset="100">
                                            Agenda
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#contato" data-hash data-hash-offset="68">
                                            Contato
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="mt-2">
                            @if(filled($mobile->value))                                    
                                @if($mobile->whatsapp)
                                    <a target="_blank" href="{{$mobile->url}}">
                                        <i class="header-icon z-whatsapp icon-whatsapp" style="font-size:36px"></i>
                                    </a>                                            
                                @endif
                            @endif
                        </div>
                        <button class="btn header-btn-collapse-nav" data-bs-toggle="collapse" data-bs-target=".header-nav-main nav">
                            <i class="fas fa-bars"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
