
@php
	$mainUrl = config('app.url');
    $hasAgenda = filled($city->google_schedule);
@endphp
<div class="header-nav pt-1">
    <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
        <nav class="collapse">
            <ul class="nav nav-pills" id="mainNav">
                <li>
                    <a data-hash class="nav-link active" href="{{$mainUrl}}"> 
                        <i class="fas fa-home"></i> &nbsp;
                    </a>
                </li>
                <li>
                    <a href="#pilares"  data-hash data-hash-offset="68"> 
                        O Projeto
                    </a>
                </li>
                <li>
                    <a href="#servicos"  data-hash data-hash-offset="100">
                        Serviços
                    </a>
                </li>
                <li>
                    <a href="#certidoes" data-hash data-hash-offset="100">
                        Certidões
                    </a>
                </li>
                <li>
                    <a href="#mei" data-hash data-hash-offset="100">
                        MEI
                    </a>
                </li>
                <li>
                    <a href="#tutoriais" data-hash data-hash-offset="100">
                        Tutoriais
                    </a>
                </li>
                @if($hasAgenda)
                    <li>
                        <a href="#agenda" data-hash data-hash-offset="100">
                            Agenda
                        </a>
                    </li>
                @endif
                <li>
                    <a href="#contato" data-hash data-hash-offset="68">
                        Contato
                    </a>
                </li>
            </ul>
        </nav>
    </div>
    <!-- <ul class="header-social-icons social-icons d-none d-sm-block">
        <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
        <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
    </ul> -->
    <button class="btn header-btn-collapse-nav" data-bs-toggle="collapse" data-bs-target=".header-nav-main nav">
        <i class="fas fa-bars"></i>
    </button>
</div>
