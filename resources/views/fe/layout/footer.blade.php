@php
	use App\Traits\FE;

	$full_address = $city->fullAddress();
    $phone = FE::getPhone($city->phone);
    $mobile = filled($city->mobile) ? FE::getPhone($city->mobile) : $phone;
	$mainUrl = config('app.url')."/".$city->getSlug();
@endphp

<div class="container">
	<div class="footer-ribbon">
		<span>Sala do Empreendedor</span>
	</div>
	<div class="row py-5 my-4">
		<!-- <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
			<h5 class="text-3 mb-3">NEWSLETTER</h5>
			<p class="pe-1">Assine a nesletter do Sebrae/TO e receba as últimas notícias, informativos diretamente em seu email</p>
			<div class="alert alert-success d-none" id="newsletterSuccess">
				<strong>Success!</strong> Email adicionado a lista com sucesso!
			</div>
			<div class="alert alert-danger d-none" id="newsletterError"></div>
			<form id="newsletterForm" action="php/newsletter-subscribe.php" method="POST" class="me-4 mb-3 mb-md-0">
				<div class="input-group input-group-rounded">
					<input class="form-control form-control-sm bg-light" placeholder="Endereço de email" name="newsletterEmail" id="newsletterEmail" type="text">
					<button class="btn btn-light text-color-dark" type="submit"><strong>Assinar!</strong></button>
				</div>
			</form>
		</div> -->
		<div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
			<h5 class="text-3 mb-3">MENU RÁPIDO</h5>
			<ul>
				<li><a href="">Home</a></li>
				<li><a href="">Serviços</a></li>
			</ul>
		</div>
		<div class="col-md-6 col-lg-3 mb-4 mb-md-0">
			<div class="contact-details">
				<h5 class="text-3 mb-3">FALE CONOSCO</h5>
				<ul class="list list-icons list-icons-lg">
					<li class="mb-1"><i class="far fa-dot-circle text-color-primary"></i>
						<p class="m-0">
							{!! $full_address !!}
						</p>
					</li>
					@if(filled($mobile->value))
						@if($mobile->whatsapp)
							<li class="mb-1">
								<i class="fab fa-whatsapp text-color-primary"></i>
								<p class="m-0">
									<a target="_blank" href="{{$mobile->url}}">
										{{$mobile->value}}
									</a>
								</p>
							</li>
						@else
							<li class="mb-1">
								<i class="fab fa-phone text-color-primary"></i>
								<p class="m-0">
									<a target="_blank" href="tel:{{$mobile->cleanValue}}">
										{{$mobile->value}}
									</a>
								</p>
							</li>
						@endif
					@endif
					@if(optional($phone)->value !== $mobile->value)
						@if(optional($phone)->whatsapp)
							<li class="mb-1">
								<i class="fab fa-whatsapp text-color-primary"></i>
								<p class="m-0">
									<a target="_blank" href="{{$phone->url}}">
										{{$phone->value}}
									</a>
								</p>
							</li>
						@else
							<li class="mb-1">
								<i class="fab fa-whatsapp z-phone text-color-primary"></i>
								<p class="m-0">
									<a target="_blank" href="tel:{{optional($phone)->cleanValue}}">
										{{$phone->value}}
									</a>
								</p>
							</li>
						@endif
					@endif
					@if($city->email)
						<li class="mb-1">
							<i class="far fa-envelope text-color-primary"></i>
							<p class="m-0">
								<a href="mailto:{{$city->email}}">
									{{$city->email}}
								</a>
							</p>
						</li>
					@endif
				</ul>
			</div>
		</div>
		<!-- <div class="col-md-6 col-lg-2">
			<h5 class="text-3 mb-3">Nos siga nas redes sociais</h5>
			<ul class="social-icons">
				<li class="social-icons-facebook"><a href="https://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
				<li class="social-icons-twitter"><a href="https://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
				<li class="social-icons-linkedin"><a href="https://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
			</ul>
		</div> -->
	</div>
</div>
<div class="footer-copyright">
	<div class="container py-2">
		<div class="row py-4">
			<div class="col-lg-4 d-flex align-items-center justify-content-center justify-content-lg-start mb-2 mb-lg-0">
				<a href="{{$mainUrl}}" class="logo pe-0 pe-lg-3">
					<img alt="Porto Website Template" src="images/front/sala-white-horizontal-small.png" 
					class="opacity-5" width="auto" height="50" data-plugin-options="{'appearEffect': 'fadeIn'}">
				</a>
			</div>
			<div class="col-lg-4 d-flex align-items-center justify-content-center justify-content-lg-start mb-4 mb-lg-0">
				<p>© Copyright 2021. Todos os Direitos Reservados ao Sebrae - TO.</p>
			</div>
			<div class="col-lg-4 d-flex align-items-center justify-content-center justify-content-lg-end">
				<!-- <nav id="sub-menu">
					<ul>
						<li><i class="fas fa-angle-right"></i><a href="page-faq.html" class="ms-1 text-decoration-none"> FAQ</a></li>
						<li><i class="fas fa-angle-right"></i><a href="sitemap.html" class="ms-1 text-decoration-none"> Mapa do Site</a></li>
						<li><i class="fas fa-angle-right"></i><a href="contact-us.html" class="ms-1 text-decoration-none"> Fale Conosco</a></li>
					</ul>
				</nav> -->
			</div>
		</div>
	</div>
</div>
