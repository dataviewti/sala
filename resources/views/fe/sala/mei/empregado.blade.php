<div class="container">

<h2 class="sub-title">MEI QUE CONTRATA EMPREGADO</h2>



<p>O microempreendedor individual pode contratar até 1 (um) funcionário, e seu salário não pode ser superior ao piso da categoria ou ao salário mínimo nacional, o empregado receberá o que for maior.</p>



<p>O empresário tem obrigação de consultar, junto ao sindicato da categoria, acordos e convenções coletivas de trabalho referente a piso salarial, vale-refeição, jornada de trabalho, vale-transporte, seguro de vida, uniformes, entre outros.</p>


<div class="col-12 col-sm-5 mx-auto">
    <table class="table table-striped">
        <thead>
            <tr>
                <th> TABELA DE CUSTOS </th>
                <th> Valor R$ </th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <td>Salário Mínimo</td>
            <td>R$ 1.045,00</td>
            </tr>
            <tr>
            <td>INSS 3%</td>
            <td>R$ 31,35</td>
            </tr>
            <tr>
            <td>FGTS 8%</td>
            <td>R$ 86,60</td>
            </tr>
        </tbody>
    </table>
    <p>
        Obs.: valor a ser recolhido mensalmente do funcionário.
    </p>
    <p class="has-text-align-right"><em>Vigente a partir de 01/01/2019</em></p>
</div>







<p><strong>PRINCIPAIS OBRIGAÇÕES:</strong></p>



<ul><li>Anotação na CTPS (Carteira de Trabalho e Previdência Social).</li><li>Preencher livro ou ficha de matricula de empregado.</li><li>Pagar o salário mensalmente, bem como férias e 13º salário.</li><li>Fornecer comprovante de pagamento de salário com os descontos legais mensalmente ao empregado.</li><li>Realizar a inscrição do empregado no Programa de Integração Social – PIS caso ele não possua.</li><li>Realizar mensalmente a prestação de informações ao eSocial.</li></ul>



<p>O empregado do MEI tem os mesmos direitos que qualquer outro trabalhador, ou seja, vai receber normalmente férias, 13º salário, aviso prévio, FGTS, horas extras, adicional noturno, entre outros.</p>



<p><strong>DOCUMENTOS NECESSÁRIOS E QUALIFICAÇÃO CADASTRAL:</strong></p>



<ul><li>CTPS.</li><li>Certificado Militar: para homens maiores de 18 anos, prova de quitação com o serviço militar.</li><li>Certidão de nascimento ou de casamento.</li><li>Declaração de dependentes para fins de imposto de renda na fonte.</li><li>Atestado médico de saúde ocupacional.</li><li>Declaração de opção ou não pelo vale-transporte.</li><li>RG, CPF, PIS, título de eleitor.</li><li>Histórico escolar.</li><li>Contrato de trabalho assinado em duas vias.</li></ul>



<p><strong>ELABORANDO O CONTRATO DE TRABALHO</strong></p>



<ul><li>Algumas informações são importantes e não podem faltar no contrato de trabalho.</li><li>Dados completos tanto do empregado quanto do empregador.</li><li>Data de admissão (início das atividades).</li><li>Data de admissão (início das atividades)..</li><li>Remuneração mensal a ser paga.</li><li>Duração do contrato de trabalho: determinado (informar a data de início e de término do contrato) ou indeterminado.</li><li>Se houver, possibilidade de mudança de local de trabalho.</li><li>&nbsp;Assinaturas das partes e de duas testemunhas.</li></ul>



<p><strong>MODALIDADES DE CONTRATO</strong></p>



<p>Após reformulação das leis trabalhistas através da Lei 13467/2017, passaram a vigorar novas modalidades de contrato de trabalho, além das já existentes. São elas: contrato por tempo indeterminado, contrato por tempo determinado, contrato de experiência, trabalho intermitente (Lei 13467/2017), autônomo exclusivo (Lei 13467/2017) e teletrabalho ou trabalho remoto (Lei 13467/2017).</p>



<p>O contrato por prazo indeterminado é mais comumente usado, é a regra geral. Pois tem data de início, contudo não tem data de encerramento do contrato de trabalho.</p>



<p>O contrato por prazo determinado tem data de início e de fim pré-estabelecidas, e não pode ter uma prazo máximo de duração superior a 2 (dois) anos, conforme determina artigo 443 da CLT.</p>



<p>Já o contrato de experiência é um contrato firmado para que empregado e empregador se conheçam antes da efetivação do contrato. Tem duração máxima de 90 dias podendo ser renovado uma única vez.</p>



<p>O contrato de trabalho intermitente permite que o funcionário exerça suas atividades de forma não contínua, alternando os períodos trabalhados e de inatividade. Podem ser determinados por hora, dia ou mês, conforme acordado com o empregado.</p>



<p>O empregado deve ser convocado ao trabalho até 3 dias antes e pode aceitar ou não a proposta. O período de inatividade não será computado como tempo a disposição do empregador, devendo esse fazer o pagamento do período efetivamente trabalhado.</p>



<p>Com a nova lei surgiu também à figura do trabalhador autônomo, profissional que pode executar suas atividades “com ou sem exclusividade, de forma contínua ou não, afastando a qualidade de empregado.” (art. 442-B Lei 13.467/2017)</p>



<p>Nessa perspectiva, o trabalhador autônomo se equipara a pessoa jurídica, descaracterizando o vínculo empregatício e isentando o contratante das responsabilidades trabalhistas.</p>



<p>O teletrabalho é uma modalidade de contrato de trabalho que permite ao funcionário exercer suas atividades total ou parcialmente, por meio de uso de tecnologias de comunicação.</p>



<p>Conforme consta no art. 75-D da Lei 13467/2017, no contrato de trabalho devem estar discriminadas quais atividades o funcionário irá prestar nessa modalidade, as responsabilidades pela aquisição, manutenção ou fornecimento dos equipamentos tecnológicos e da infraestrutura necessária e adequada à prestação do trabalho remoto, bem como ao reembolso de despesas arcadas pelo empregado.</p>



<p><strong>CONTRATANDO O FUNCIONÁRIO</strong></p>



<p>Recomenda-se que o MEI que quer registrar um funcionário contrate o serviço de um contador, que vai prestar toda a assessoria necessária referente a departamento de pessoal.</p>



<p><strong>Passos para a contratação:</strong></p>



<ol><li><strong>Consulta Qualificação Cadastral:</strong>&nbsp;inclusão do trabalhador no sistema eSocial só ocorrerá se houver compatibilidade entre a base do Cadastro de Pessoa Física – CPF com a base do Número de Identificação Social – NIS. A consulta é feita pelo link a seguir:&nbsp;<a href="http://www.esocial.gov.br/">http://www.esocial.gov.br</a>&nbsp;&gt; Consulta Qualificação Cadastral.<br> Após a verificação cadastral, o aplicativo retornará o resultado para o usuário informando quais os campos estão com divergências.<ul><li>Divergências relativas ao CPF (situação “suspenso”, “nulo” ou “cancelado”, nome ou data de nascimento divergente) – o aplicativo apresentará a mensagem orientativa de onde deverá requisitar a alteração dos dados</li><li>Divergências relativas ao NIS (CPF ou data de nascimento divergente) – o usuário deverá estar atento, pois a orientação será dada de acordo com o ente responsável pelo cadastro do NIS (INSS, CAIXA ou BANCO DO BRASIL).</li></ul></li><li><strong>Exame médico:</strong>&nbsp;O empregador deverá solicitar que o candidato realize o exame médico de saúde ocupacional, é por meio deste que se verifica a saúde física e mental do candidato. É obrigatório e deve ser custeado pelo empregador.</li><li><strong>Registro na CTPS:</strong> o empregador deverá anotar na CTPS do funcionário a data de admissão, salário, cargo, função e condições especiais de contratação se houver. O empregador deverá fazer a anotação e devolver a CTPS devidamente assinada para o funcionário em até 48 horas contadas da contratação.</li><li><strong>Contribuição previdenciária ao INSS (Instituto Nacional do Seguro Social):</strong>&nbsp;o funcionário pagará de 8% a 11% do seu salário ao INSS enquanto que o MEI irá recolher 3% do salário do seu funcionário referente à cota patronal de contribuição ao INSS.</li></ol>

<div class="col-12 col-sm-5 mx-auto">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">Salário de Contribuição</th>
                <th scope="col">Alíquota de INSS</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Até R$ 1.751,81</td>
                <td>8%</td>
            </tr>
            <tr>
                <td>De 1.715,82 até 2.919,72</td>
                <td>9%</td>
            </tr>
            <tr>
                <td>De 2.919,73 até 5.839,45</td>
                <td>11%</td>
            </tr>
        </tbody>
    </table>
    <p class="has-text-align-right"><em>Vigente a partir de 01/01/2019</em></p>
</div>







<p>Ou seja, todo mês o MEI deverá descontar do funcionário de 8% a 11% do salário do mês e pagar através de Guia de Recolhimento Previdenciário junto com a cota patronal de 3%.</p>



<ol start="5"><li><strong>&nbsp;Cadastro PIS:</strong>&nbsp;caso o empregado não possua matricula, o empregador deverá efetuar o cadastro para seu funcionário.</li><li><strong>Vale transporte:</strong>&nbsp;o MEI deverá fornecer vale-transporte para o deslocamento exclusivo do funcionário da sua residência até o local de trabalho.</li></ol>



<p>Poderá ser descontado do funcionário até 6% do salário do empregado, limitado ao valor integral do vale-transporte. Recomenda-se que o vale-transporte seja adquirido por meio de postos autorizados e posteriormente entregue ao funcionário.</p>



<ol start="7"><li><strong>Vale-refeição:</strong>&nbsp;Cabe à empresa observar as convenções coletivas de trabalho para verificar a obrigatoriedade ou não de fornecer o vale-refeição ao empregado.</li><li><strong>Normas reguladoras do trabalho – NRs:</strong>&nbsp;as NRs determinam algumas obrigações que o empregador deve cumprir para zelar pela segurança e bem estar do empregado.</li></ol>



<p>A transmissão das informações ao CAGED (Cadastro Geral de Empregados e Desempregados), FGTS (Fundo de Garantia por Tempo de Serviço) e RAIS (Relação Anual de Informações Sociais) serão prestadas utilizando o sistema de informações unificado do eSocial.</p>



<p><strong>O MICROEMPREENDEDOR INDIVIDUAL E O ESOCIAL</strong></p>



<p>O Sistema de Escrituração Digital das Obrigações Fiscais, Previdenciárias e Trabalhistas (eSocial) visa unificar em um único ambiente a prestação de informações pelo empregador em relação aos seus trabalhadores.</p>



<p>Em se tratando de MEI, se faz necessário a prestação de informações ao eSocial apenas para o empresário que possuir empregado. O MEI fica dispensado do auxílio de contador para cumprimento dessa obrigação, contudo a contratação da figura do profissional de contabilidade como responsável técnico não é impeditivo. As informações são transmitidas através de código de acesso ou via certificado digital.</p>



<p>As informações ao eSocial serão prestadas a partir de 16 de julho de 2018, conforme demonstrado abaixo:</p>



<table class="table table-striped">
  <tbody>
  <tr>
    <td>A partir de 16 de julho de 2018</td>
    <td>A partir de 16 de julho de 2018</td>
  </tr>
  <tr>
    <td>A partir de 10 de outubro de 2018</td>
    <td>Dados do empregado do MEI e seus vínculos com as empresas (eventos periódicos e não periódicos)</td>
  </tr>
  <tr>
    <td>A partir de 10 de janeiro de 2019</td>
    <td>Folha de Pagamento</td>
  </tr>
  <tr>
    <td>A partir de abril de 2019</td>
    <td>Substituição da GFIP para recolhimento de contribuições previdenciárias</td>
  </tr>
  <tr>
    <td>A partir de abril de 2019</td>
    <td>Substituição da GFIP para recolhimento de FGTS.</td>
  </tr>
  <tr>
    <td>A partir de janeiro de 2020</td>
    <td>Dados de segurança e saúde do trabalhador</td>
  </tr>
</tbody>
</table>



<p class="has-text-align-center"><em>Fonte: Portal eSocial, 2018.</em></p>



<p>Durante a implantação do eSocial o MEI não será penalizado, caso não consiga cumprir os prazos, visto que as informações foram flexibilizadas. Ele tem até a terceira fase para atender às duas primeiras.</p>



<p>Esta flexibilização é válida apenas durante a implantação inicial do sistema, para permitir que todos se ajustem. Após esta fase, passam a valer os prazos previstos no MOS (Manual de Orientação do eSocial) para cada evento.</p>



<p>Quadro explicativo de quanto o MEI vai ter de custo por mês com o funcionário</p>



<div class="col-12 col-sm-5 mx-auto">
    <table class="table table-striped">
        <tbody>
            <tr>
                <td>Salário base</td>
                <td><span>R$ 1.045,00</span></td>
            </tr>
            <tr>
                <td>INSS parte do empregado 8%</td>
                <td>R$ 83,60</td>
            </tr>
            <tr>
                <td>NSS patronal – 3%</td>
                <td>R$ 31,35</td>
            </tr>
            <tr>
                <td>FGTS – 8%</td>
                <td>R$ 83,60</td>
            </tr>  	  	
            <tr>
                <td>Vale transporte – 6%</td>
                <td>R$ 62,60</td>
            </tr>  	
        </tbody>
    </table>
</div>



<p>Por exemplo, se o empregado usa 2 (dois) vale-transporte por dia a um custo de R$ 3,80 cada passagem, durante 22 dias trabalhados no mês (descontando sábados e domingos). O valor total do custo com transporte é:</p>



<p class="has-text-align-center"><em>R$ 4,30 x 2&nbsp;<strong>=&gt;</strong>&nbsp;R$ 8,60 x 22 dias&nbsp;<strong>=&gt;</strong>&nbsp;<strong>R$ 189,20&nbsp;</strong>(valor total do mês)</em><br><strong>Custo do VT do funcionário</strong></p>



<p class="has-text-align-left">A empresa vai descontar do funcionário no máximo 6% do seu salário, o restante do valor será custeado pela empresa. Ou seja, nesse caso fica da seguinte forma:</p>



<p class="has-text-align-center">R$ 189,20 – R$ 62,70 =&gt;&nbsp;<strong>R$ 126,50</strong><br><strong>Lembrando que na mesma guia que será recolhido o INSS patronal também será recolhido o INSS do funcionário.</strong></p>



<p>No caso a folha de pagamento do funcionário ficaria da seguinte forma:</p>



<div class="col-12 col-sm-5 mx-auto">
    <table class="table table-striped">
        <tbody>
            <tr>
                <td>Valor Bruto</td>
                <td>R$ 1.045,00</td>
            </tr>
            <tr>
                <td>(-) INSS (8%)</td>
                <td>R$ 83,60</td>
            </tr>
            <tr>
                <td>(-) VT (6%) </td>
                <td>R$ 62,70</td>
            </tr>
            <tr>
                <td><strong>Valor líquido a pagar</strong></td>
                <td><strong>R$ 898,70</strong></td>
            </tr>
        </tbody>
    </table>
</div>


<div class="col-12 col-sm-5 mx-auto">
    <table class="table table-striped">
        <thead>
            <tr>
                <td scope="col"><strong>Encargos</strong></td>
                <td scope="col"><strong>Valor R$</strong></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>INSS patronal</td>
                <td>R$ 31,35</td>
            </tr>
            <tr>
                <td>INSS parte empregado</td>
                <td>R$ 83,60</td>
            </tr>
            <tr>
                <td>(+) FGTS</td>
                <td>R$ 83,60</td>
            </tr>
            <tr>
                <td>(+) Vale transporte</td>
                <td>R$ 189,20</td>
            </tr>
            <tr>
                <td><strong>TOTAL</strong></td>
                <td><strong>R$ 387,75</strong></td>
            </tr>
        </tbody>
    </table>
    <p class="has-text-align-center"><strong>Custo Total do MEI com funcionário:</strong><br><strong>R$ 898,70 + R$ 387,75 = R$ 1.286,45</strong></p>
</div>





<p></p>
</div>
