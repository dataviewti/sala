@php
    $cnaes = \DB::table('cnaes')->select()->orderBy('activity')->get();
@endphp
<div class="row">
    <div>
        <!-- <p>Anexo XIII da Resolução CGSN nº 94, de 29 de novembro de 2011. (Alterado pela Resolução CGSN nº 104, de 12 de dezembro de 2012) (Vide art. 5º da Res. CGSN nº 104/2012)</p>    -->
        <p>
            <strong>
                Tabela com atividades permitidas no MEI atualizada 2021
            </strong>
        </p>
        <!-- <a href="#" class="mb-1 mt-1 me-1 btn btn-tertiary"><i class="fas z-link ms-1"></i> <strong>Tabela CNAE Completa</strong></a> -->
    </div>

    <div class="col-sm-12 mt-3">
        <table id="ocupacoesTable" class="table table-striped table-bordered responsive dt-responsive" style="width:100%">
            <thead>
                <tr>
                    <th>CNAE</th>
                    <th>Atividade</th>
                    <th>Descrição</th>
                    <th>ISS</th>
                    <th>ICMS</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cnaes as $c)
                    <tr>
                        <td style="width:40px">{{$c->cnae}}</th>
                        <td>{{$c->activity}}</th>
                        <td>{{$c->description}}</th>
                        <td>{{$c->iss}}</th>
                        <td>{{$c->icms}}</th>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>