<div class="container">
    <p>
        <strong>1. INFORMAÇÕES GERAIS</strong>
    </p>
    <p>
        Microempreendedor Individual – MEI pode ser uma ótima alternativa para quem busca uma oportunidade de negócio próprio ou atua na informalidade.
    </p>
    <p>
        Entenda um pouco mais sobre esta nova figura jurídica.
    </p>

    <p>
        <strong>1.1. Desburocratização</strong>
    </p>
    <p>
        O MEI é uma nova figura jurídica referenciada pelo Decreto estadual 52.228, de 05 de outubro de 2007, e posteriormente regulada pela Lei Complementar federal 123, de 14 de dezembro de 2006 , na redação que lhe foi dada pela Lei Complementar federal 128, de 19 de dezembro de 2008, com as alterações feitas pela Lei Complementar nº 133, de 28 de dezembro de 2009.
    </p>
    <p>
        Além da redução da carga tributária, o MEI foi contemplado com uma série de vantagens para reduzir a burocracia, tanto na apuração quanto no pagamento de tributos, bem como em relação aos mecanismos de formalização (registro empresarial, inscrições fiscais e licenciamento da atividade).
    </p>

    <p>
        <strong>1.2. Definição</strong>
    </p>
    <p>
        MEI é o empresário individual (que não tem sócio), com faturamento anual de até R$ 81 mil, optante pelo Simples Nacional.
    </p>

    <p>
        <strong>1.3. Condições para se tornar MEI</strong>
    </p>
    <ul>
        <li>
            Tenha obtido faturamento no ano anterior de até R$ 81.000,00 (oitenta e um mil reais), ou R$ 6.750,00 (seis mil, setecentos e cinquenta reais) por mês no caso de início de atividade;
        </li>
        <li>
            Seja optante pelo Simples Nacional;
        </li>
        <li>
            Exerça as atividades permitidas para o MEI . Saiba mais em 
            <a href="#mei" data-toggle="tab" onclick="setTab('#nav-ocupacoes')"><strong>Ocupações</strong></a>. 
        </li>
        <li>
            Possua um único estabelecimento;
        </li>
        <li>
            Não participe de outra empresa como titular, sócio ou administrador;
        </li>
        <li>
            Tenha, no máximo, um empregado com remuneração de um salário mínimo ou piso da categoria (o que for maior);
        </li>
        <li>
            Cuja atividade seja permitida no local pretendido, segundo a legislação municipal.
        </li>
    </ul>


    <p>
        <strong>1.4. Atividades permitidas para o MEI</strong>
    </p>
    
    <p>
        Há mais de 400 ocupações que podem ser formalizadas por meio da figura do MEI. São elas, basicamente:
    </p>

    <ul>
        <li>
            Comércio em geral;
        </li>
        <li>
            Indústria em geral (poucas exceções);
        </li>
        <li>
            Serviços de natureza não intelectual, tais como: alfaiate, animador de festas, artesão, barbeiro, borracheiro, cabeleireira, carpinteiro, catador de resíduos recicláveis, costureira, digitador, encanador, engraxate, fotógrafo, funileiro, instrutor de idiomas, jardineiro, manicure/pedicure, marceneiro, mecânico de veículos, motoboy, pedreiro, professor particular, sapateiro, tapeceiro, etc. 
        </li>
    </ul>
    
    <p>
        Consulte a lista completa das atividades permitidas ao MEI na guia Ocupações ou 
        <a href="#mei" data-toggle="tab" onclick="setTab('#nav-ocupacoes')"><strong>clicando aqui</strong></a>. 
    </p>


    <p>
        <strong>1.5. Tributos</strong>
    </p>
    
    <p>
        A grande novidade do MEI vem justamente da isenção de praticamente todos os tributos. Paga-se apenas uma taxa fixa mensal de 5% do salário mínimo vigente (R$ 47,70) a título de contribuição previdenciária ao INSS, R$ 1,00 de ICMS para o Estado (se a atividade for comércio/indústria), e/ou R$ 5,00 de ISS para o município (se a atividade for prestação de serviço).
    </p>

    <p>
        Valores a serem recolhidos mensalmente a partir de maio/2011, de acordo com a MP 529/2011:
    </p>

    <div class="col-12 col-sm-5 mx-auto">
        <table class="table table-striped">
            <thead>
                <tr>
                    <td scope="col"><strong>Valor R$</strong></td>
                    <td scope="col"><strong>Tipo de atividade</strong></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>R$ 57,25</td>
                    <td>Comércio e/ou Indústria (INSS + ICMS)</td>
                </tr>
                <tr>
                    <td>R$ 53,25</td>
                    <td>Prestação de Serviços (INSS + ISS)</td>
                </tr>
                <tr>
                    <td>R$ 58,25</td>
                    <td>Atividades mistas (INSS + ICMS + ISS)</td>
                </tr>
                <tr>
                    <td>R$ 52,25</td>
                    <td>Atividades isentas de ICMS e ISS</td>
                </tr>
            </tbody>
        </table>
    </div>

    <p>
        O pagamento desses valores será feito por meio de um documento chamado Documento de Arrecadação do Simples Nacional(DAS), que é gerado pela Internet, no Portal do Empreendedor. O pagamento será feito na rede bancária e casas lotéricas, até o dia 20 de cada mês. EMISSÃO DE GUIA(S)
    </p>

    <p>
        <strong>1.6. Principais benefícios</strong>
    </p>

    <p>
        Os benefícios associados à formalização do empreendedor por meio da figura do MEI podem assim ser exemplificados:
    </p>

    <ul>
        <li>
            Contratação de um empregado com menor custo
        </li>
        <li>
            Isenção de taxas para o registro da empresa
        </li>
        <li>
            Menos burocracia
        </li>
        <li>
            Facilitação do acesso ao crédito
        </li>
        <li>
            Redução da carga tributária
        </li>
        <li>
            Controles muito simplificados
        </li>
        <li>
            Assessoria gratuita
        </li>
    </ul>



    <p>
        <strong>1.7. Obrigações Acessórias</strong>
    </p>

    <p>
        Ao se formalizar o MEI também assume algumas obrigações, tais como:
    </p>
    <p>
        Nota Fiscal: O MEI está dispensado de emitir Nota Fiscal nas operações com pessoas físicas. Somente estará obrigado a emitir Nota Fiscal nas operações que realizar com pessoas jurídicas.
    </p>

    <a href="#mei" data-toggle="tab" onclick="setTab('#nav-duvidas')"><strong>Mais informações em Perguntas e Respostas.</strong></a>. 

    <p>
        Declaração Anual Simplificada (DASN SIMEI): Anualmente, deverá fazer uma declaração do faturamento, também pela Internet, e nada mais. Essa declaração deverá ser feita até o último dia útil de maio de cada ano.
    </p>
    <p>
        Relatório Mensal de Receitas Brutas: O MEI deverá elaborar mensalmente um relatório de receitas mensais, somando o total de mercadorias comercializadas e/ou serviços prestados com pessoas físicas e/ou jurídicas. Toda mercadoria adquirida pelo MEI deverá ser acompanhada de notas fiscais, que deverão ser anexadas ao relatório mensal.
    </p>
    <p>
        MEI que contrata empregado: O MEI que contratar empregado deverá ficar atento às seguintes obrigações: salário, 13º salário, férias, 1/3 férias, FGTS, INSS (8% – empregado e 3% – empregador), Contribuição Sindical, vale-transporte, salário família, CAGED, RAIS, PIS.
    </p>
    <p>
        É preciso observar ainda a Convenção Coletiva do Sindicato da categoria. A fim de se evitar o descumprimento da lei e consequentes atrasos e multas, recomenda-se que tais serviços sejam prestados por profissional de contabilidade. 
    </p>


    <p>
        <strong>1.8 Como se formalizar</strong>
    </p>

    <p>
        Compareça a Sala do Empreendedor com os seguintes documentos:
    </p>
    
    <ol>
        <li>
            RG E CPF – (ORIGINAL E 2 CÓPIAS)
        </li>
        <li>
            Cópia do comprovante de endereço comercial
        </li>
        <li>
            Cópia do comprovante de endereço residencial
        </li>
        <li>
            Caso declarou Imposto de Renda Física nos últimos 2 anos, trazer a cópia do Recibo de entrega de declaração de imposto de renda 
        </li>
        <li>
            Cópia do Título de eleitor
        </li>
    </ol>

    <p>
        obs.: os comprovantes devem ser atualizados. 
    </p>

    <p>
        A inscrição no portal é muito simples, devendo o empreendedor informar, basicamente, em uma única tela, o número do CPF, da identidade, o número do título de eleitor ou o número do recibo da última declaração de imposto de renda, o CEP, nome, endereço residencial e/ou comercial e a atividade a ser exercida. Após o cadastramento, o CNPJ e o Número de Inscrição de Registro de Empresa (NIRE) são obtidos imediatamente. Não é necessário encaminhar nenhum documento à Junta Comercial.
    </p>
    <p>
        Efetuada a inscrição do MEI, os dados cadastrais serão disponibilizados para a Previdência Social e demais órgãos e entidades responsáveis pela inscrição fiscal.
    </p>
    <p>
        Feito o registro do MEI, será disponibilizado no Portal do Empreendedor o documento Certificado da Condição de Microempreendedor Individual – CCMEI para consulta por qualquer interessado.
    </p>
    <p>
        Atenção: O procedimento junto ao Portal do Empreendedor não garante a formalização integral do MEI. Há outras exigências ainda não integradas ao Portal. Saiba mais sobre em “menu direcionado com informações”.    
    </p>
</div>