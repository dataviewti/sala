@php
    $questions = \DB::table('questions')
        ->select()
        ->orderBy('order','asc')
        ->get();
@endphp
<div class="row">
    <div class='col-12'>
        @foreach($questions as $q)
            <div class="toggle toggle-primary" data-plugin-toggle="">
                <section class="toggle">
                    <a class="toggle-title">{{$q->question}}</a>
                    <div class="toggle-content px-3" style="display: none;">
                    {!!$q->answer!!}
                    </div>
                </section>
            </div>
        @endforeach
    </div>
</div>