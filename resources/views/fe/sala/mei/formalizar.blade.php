<!-- <div class="container">
        
<p>O Município de Ponta Grossa, atendendo a Lei da Liberdade Econômica nº 13.874, de 20 de setembro de 2019 e regulamentada pela Resolução nº 59 de 12/08/2020 do CGSIM, dispensa o Microempreendedor Individual do Alvará de Localização e Funcionamento, mediante declaração no&nbsp;Termo de Ciência e Responsabilidade com Efeito de Dispensa de Alvará e Licença de Funcionamento&nbsp;feita pelo empresário no momento da formalização no Portal do Empreendedor.</p>
<p>Para atividades que executadas em ponto físico (loja de roupas, por exemplo), apesar do MEI estar dispensado de realizar a consulta prévia antes da formalização, recomenda-se que o empresário o faça, para verificar junto ao Município de Ponta Grossa a viabilidade de exercer a atividade desejada no endereço informado, bem como as exigências necessárias, evitando minimizar riscos de investimento em local impossibilitado de instalação do empreendimento.</p>
<p><strong>CONSULTA DE VIABILIDADE</strong></p>
<p>A pesquisa prévia de viabilidade para o MEI consiste em verificar se a atividade pretendida pode ser exercida no local desejado.A Sala Digital disponibiliza de forma online e gratuita tutorial com o passo a passo de como solicitar a consulta prévia sem sair de casa. Acesse o tutorial pelo link abaixo:</p>

<a href="#" class="mb-1 mt-1 me-1 btn btn-primary"><i class="fas fa-download ms-1"></i> <strong>Tutorial sobre o tema</strong></a>


<p></p>
<p>Após o deferimento da consulta prévia, o empresário já pode fazer o seu registro como MEI.</p>
<p><strong>FORMALIZAÇÃO</strong></p>
<p>A formalização do Microempreendedor Individual é realizada de forma online e gratuita direto pelo Portal do Empreendedor. A Sala Digital de Ponta Grossa&nbsp;disponibiliza tutorial com o passo a passo da formalização do MEI. Acesse o tutorial pelo link abaixo:</p>

<a href="#" class="mb-1 mt-1 me-1 btn btn-primary"><i class="fas fa-download ms-1"></i> <strong>Tutorial sobre o tema</strong></a>


<p></p>
<p>O processo de formalização do MEI&nbsp;é realizado de forma 100% online e gratuita. A Sala do Empreendedor de Ponta Grossa oferece aos empresários, suporte durante o processo através dos canais de atendimento online:&nbsp;Chat&nbsp;na página da Sala Digital e WhatsApp (das 12:00 as 17:00h).&nbsp;</p>
<p><strong>LEGALIZAÇÃO</strong></p>
<p>De acordo com a Lei da Liberdade Econômica nº 13.874, de 20 de setembro de 2019 e regulamentada pela Resolução nº 59 de 12/08/2020 do CGSIM, o Microempreendedor Individual é dispensado de atos públicos e por esse motivo está dispensado de solicitar o alvará de localização/funcionamento.</p>
<p>Entretanto, isso não quer dizer que o empresário não precisa estar adequado com as exigências municipais. O não cumprimento das exigências pode acarretar na cassação de seu registro como Microempreendedor Individual e baixa do mesmo.</p>
<p>Para saber quais são as exigências para sua atividade, basta fazer a consulta prévia (já mencionado acima) para verificar o que precisar ser adequado no estabelecimento.</p>

</div> -->

<div class="container">
    <p>
        <strong>FORMALIZAÇÃO</strong>
    </p>

    <p>
        A formalização do Microempreendedor Individual é realizada de forma online e gratuita direto pelo Portal do Empreendedor. A Sala Digital de Ponta Grossa disponibiliza tutorial com o passo a passo da formalização do MEI. Acesse o tutorial pelo link abaixo:
    </p>

    <a target = '_blank' href="http://sala/documents/download/como-formalizar-o-mei-99.pdf" class="mb-1 mt-1 me-1 btn btn-primary"><i class="fas fa-download ms-1"></i> <strong>Tutorial de formalização do MEI</strong></a>

    <p>
        O processo de formalização do MEI é realizado de forma 100% online e gratuita. A Sala do Empreendedor Virtual oferece aos empresários, suporte durante o processo através dos canais de atendimento online: Chat na página da Sala do Empreendedor Virtual e WhatsApp (das 08:00 às 13:00h). 
    </p>

    <p>
        <strong>LEGALIZAÇÃO</strong>
    </p>

    <p>
        De acordo com a Lei da Liberdade Econômica nº 13.874, de 20 de setembro de 2019 e regulamentada pela Resolução nº 59 de 12/08/2020 do CGSIM, o Microempreendedor Individual é dispensado de atos públicos e por esse motivo está dispensado de solicitar o alvará de localização/funcionamento.
    </p>
    <p>
        Entretanto, isso não quer dizer que o empresário não precisa estar adequado com as exigências municipais. O não cumprimento das exigências pode acarretar na cassação de seu registro como Microempreendedor Individual e baixa do mesmo.
    </p>
    <p>
        Para saber quais são as exigências para sua atividade, basta fazer a consulta prévia (já mencionada acima) para verificar o que precisa ser adequado no estabelecimento.
    </p>


    <!-- <a href="#" class="mb-1 mt-1 me-1 btn btn-primary"><i class="fas fa-download ms-1"></i> <strong>Tutorial sobre o tema</strong></a> -->

</div>