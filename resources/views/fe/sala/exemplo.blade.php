<div class="container appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300" style="animation-delay: 300ms;">
					<div class="row py-5 my-5">
						<div class="col-md-6 order-2 order-md-1 text-center text-md-start">
							<div class="owl-carousel owl-theme nav-style-1 nav-center-images-only stage-margin mb-0 owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'responsive': {'576': {'items': 1}, '768': {'items': 1}, '992': {'items': 2}, '1200': {'items': 2}}, 'margin': 25, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}" style="height: auto;">
								
								
								
								
								
							<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0.25s ease 0s; width: 1308px; padding-left: 40px; padding-right: 40px;"><div class="owl-item active" style="width: 220.5px; margin-right: 25px;"><div>
									<img class="img-fluid rounded-0 mb-4" src="img/team/team-1.jpg" alt="">
									<h3 class="font-weight-bold text-color-dark text-4 mb-0">John Doe</h3>
									<p class="text-2 mb-0">CEO</p>
								</div></div><div class="owl-item active" style="width: 220.5px; margin-right: 25px;"><div>
									<img class="img-fluid rounded-0 mb-4" src="img/team/team-2.jpg" alt="">
									<h3 class="font-weight-bold text-color-dark text-4 mb-0">Jessica Doe</h3>
									<p class="text-2 mb-0">CEO</p>
								</div></div><div class="owl-item" style="width: 220.5px; margin-right: 25px;"><div>
									<img class="img-fluid rounded-0 mb-4" src="img/team/team-3.jpg" alt="">
									<h3 class="font-weight-bold text-color-dark text-4 mb-0">Chris Doe</h3>
									<p class="text-2 mb-0">DEVELOPER</p>
								</div></div><div class="owl-item" style="width: 220.5px; margin-right: 25px;"><div>
									<img class="img-fluid rounded-0 mb-4" src="img/team/team-4.jpg" alt="">
									<h3 class="font-weight-bold text-color-dark text-4 mb-0">Julie Doe</h3>
									<p class="text-2 mb-0">SEO ANALYST</p>
								</div></div><div class="owl-item" style="width: 220.5px; margin-right: 25px;"><div>
									<img class="img-fluid rounded-0 mb-4" src="img/team/team-5.jpg" alt="">
									<h3 class="font-weight-bold text-color-dark text-4 mb-0">Robert Doe</h3>
									<p class="text-2 mb-0">DESIGNER</p>
								</div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"></button><button type="button" role="presentation" class="owl-next"></button></div><div class="owl-dots disabled"></div></div>
						</div>
						<div class="col-md-6 order-1 order-md-2 text-center text-md-start mb-5 mb-md-0">
							<h2 class="text-color-dark font-weight-normal text-6 mb-2 pb-1">Meet <strong class="font-weight-extra-bold">Our Team</strong></h2>
							<p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit massa enim. Nullam id varius nunc.</p>
							<p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim. Nullam id varius nunc. Vivamus bibendum magna ex, et faucibus lacus venenatis eget.</p>
							<a href="page-team.html" class="btn btn-dark font-weight-semibold rounded-0 px-5 btn-py-2 text-2">LEARN MORE</a>
						</div>
					</div>
				</div>