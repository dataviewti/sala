@php
	use App\Models\City;

	$cities = collect(City::select('name','status','short_name')
		->orderBy('name')
		->where('status',1)
		->get());

@endphp


<section id="municipios" class="section section-height-3 section bg-color-grey-scale-1 border-0 m-0
 border-0 m-0 appear-animation" data-appear-animation="fadeIn">
	<div class="container pt-5 pb-3">
		<div class="row">
			<div class="col text-center">
				<h3 class="font-weight-bold text-color-default line-height-1 text-4 ls-0 mb-1">Municípios</h3>
				<h2 class="text-color-dark font-weight-bold text-8">Conheça os municípios que já possuem a Sala do Empreendedor Virtual</h2>
			</div>
		</div>

		<!-- <p>
			<input type = "text" paceholder="pesquise por nome do município"/>
		</p> -->

		<div class="row col-12 col-sm-10 d-flex justify-content-center mx-auto mt-3">

		@foreach ($cities->chunk(8) as $chunk)
			<div class="col-12 col-sm-4 shrink">
				@foreach ($chunk as $city)
				<a href="{{$city->getSlug()}}" class="d-flex my-auto">
					<h4 class="">
						{{$city->name}}
					</h4>
				</a>
				@endforeach
			</div>
		@endforeach
		</div>
	</div>
</section>
