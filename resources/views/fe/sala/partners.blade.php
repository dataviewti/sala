@php

	$partners = [
		[
			"name"=>"logo-sebrae.png",
			"order"=>1,
			"url"=>"https://portaldeservicos.to.sebrae.com.br/portaldeservicos/"
		],
		[	
			"name"=>"jucetins.png",
			"order"=>2,
			"url" => "https://www.to.gov.br/jucetins"
		],
	];


@endphp

<section class="section section-with-shape-divider border-0 m-0" id ="partners">
	<div class="shape-divider shape-divider-reverse-x" style="height: 120px;">
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 2000 120" preserveAspectRatio="xMinYMin">
			<polygon fill="#FFF" points="-11,2 693,112 2019,6 2019,135 -11,135 "></polygon>
		</svg>
	</div>
	<!-- <div class="shape-divider shape-divider-bottom shape-divider-reverse-y" style="height: 120px;">
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 2000 120" preserveAspectRatio="xMinYMin">
			<polygon fill="#FFF" points="-11,2 693,112 2019,6 2019,135 -11,135 "></polygon>
		</svg>
	</div> -->
	<div class="container py-5 my-5">
		<div class="row mb-5">
			<div class="col">
				<div class="overflow-hidden">
					<h2 class="text-color-primary font-weight-medium positive-ls-3 text-4 mb-0 appear-animation animated maskUp appear-animation-visible" data-appear-animation="maskUp" data-appear-animation-delay="200" style="animation-delay: 200ms;">PARCEIROS</h2>
				</div>
				<div class="overflow-hidden mb-3">
					<h3 class="font-weight-bold text-transform-none text-9 line-height-2 mb-0 appear-animation animated maskUp appear-animation-visible" data-appear-animation="maskUp" data-appear-animation-delay="400" style="animation-delay: 400ms;">Conheça os principais parceiros da Sala do Empreendedor</h3>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col col-12 mx-auto">
				<div class="owl-carousel owl-theme stage-margin nav-style-1 owl-loaded owl-drag owl-carousel-init" 
				data-plugin-options="{'items': 2, 'margin': 10, 'loop': true, 'nav': true, 'dots': false, 'stagePadding': 40}" 
				style="height: auto;">
					<div class="owl-stage-outer">
						<div class="owl-stage" style="transform: translate3d(-174px, 0px, 0px); transition: all 0.25s ease 0s; width: 1824px; padding-left: 40px; padding-right: 40px;">
							@foreach($partners as $p)
								<a href="{{$p['url']}}" target="_blank" class="owl-item partners-item" style="background-image:url(/images/front/sala/partners/{{$p['name']}})" data-rm-from-transition="true">
								</a>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>