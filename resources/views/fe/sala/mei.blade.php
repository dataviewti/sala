<div class="container my-3" id="mei">
    <div class="row my-5">
        <div class="col text-center appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">
            <h2 class="font-weight-bold mb-2">MEI - Microempreendedor Individual</h2>
            <p class="opacity-7">Entenda tudo sobre o MEI</p>
        </div>
    </div>


    <div class="row">
        <div class="col">
            <div class="tabs tabs-bottom tabs-center tabs-simple">
                <ul class="nav nav-tabs">
                    <li class="nav-item active">
                        <a class="nav-link active" href="#nav-entenda" data-bs-toggle="tab">
                            <span class="featured-boxes featured-boxes-style-6 p-0 m-0">
                                <span class="featured-box featured-box-primary featured-box-effect-6 p-0 m-0">
                                    <span class="box-content p-0 m-0">
                                        <i class="icon-featured z-info"></i>
                                    </span>
                                </span>
                            </span>
                            <p class="mb-0 pb-0">Entenda o que é o MEI</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#nav-formalizar" data-bs-toggle="tab">
                            <span class="featured-boxes featured-boxes-style-6 p-0 m-0">
                                <span class="featured-box featured-box-primary featured-box-effect-6 p-0 m-0">
                                    <span class="box-content p-0 m-0">
                                        <i class="icon-featured z-store"></i>
                                    </span>
                                </span>
                            </span>
                            <p class="mb-0 pb-0">Como se formalizar</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#nav-ocupacoes" data-bs-toggle="tab">
                            <span class="featured-boxes featured-boxes-style-6 p-0 m-0">
                                <span class="featured-box featured-box-primary featured-box-effect-6 p-0 m-0">
                                    <span class="box-content p-0 m-0">
                                        <i class="icon-featured z-tools"></i>
                                    </span>
                                </span>
                            </span>
                            <p class="mb-0 pb-0">Ocupações</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#nav-duvidas" data-bs-toggle="tab">
                            <span class="featured-boxes featured-boxes-style-6 p-0 m-0">
                                <span class="featured-box featured-box-primary featured-box-effect-6 p-0 m-0">
                                    <span class="box-content p-0 m-0">
                                        <i class="icon-featured z-help"></i>
                                    </span>
                                </span>
                            </span>
                            <p class="mb-0 pb-0">Principais dúvidas</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#nav-empregado" data-bs-toggle="tab">
                            <span class="featured-boxes featured-boxes-style-6 p-0 m-0">
                                <span class="featured-box featured-box-primary featured-box-effect-6 p-0 m-0">
                                    <span class="box-content p-0 m-0">
                                        <i class="icon-featured z-users"></i>
                                    </span>
                                </span>
                            </span>
                            <p class="mb-0 pb-0">MEI que contrata empregado</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" target = '_blank' href ="http://sala/documents/download/cartilha-do-mei-100.pdf">
                            <span class="featured-boxes featured-boxes-style-6 p-0 m-0">
                                <span class="featured-box featured-box-tertiary featured-box-effect-6 p-0 m-0">
                                    <span class="box-content p-0 m-0">
                                        <i class="icon-featured z-download"></i>
                                    </span>
                                </span>
                            </span>
                            <p class="mb-0 pb-0">Baixar a Cartilha do MEI</p>
                        </a>
                    </li>
                </ul>
                <div class="tab-content px-2">
                    <div class="tab-pane active" id="nav-entenda">
                        @include('fe.sala.mei.entenda')
                    </div>
                    <div class="tab-pane" id="nav-formalizar">
                        @include('fe.sala.mei.formalizar')
                    </div>
                    <div class="tab-pane" id="nav-ocupacoes">
                        @include('fe.sala.mei.ocupacoes')
                    </div>
                    <div class="tab-pane" id="nav-duvidas">
                        @include('fe.sala.mei.duvidas')
                    </div>
                    <div class="tab-pane" id="nav-empregado">
                        @include('fe.sala.mei.empregado')
                    </div>
                    <!-- <div class="tab-pane" id="nav-municipio">
                        @include('fe.sala.mei.municipio')
                    </div> -->
                </div>
            </div>
        </div>
    </div>

</div>
