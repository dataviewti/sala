<header id="header" class="header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 80}">
	<div class="header-body">
		<div class="header-container container">
			<div class="header-row">
				<div class="header-column">
					<div class="header-row">
						<div class="header-logo header-logo-sticky-change d-none d-sm-block" style="width: auto; height: 110px;">
							<a href="{{config('url')}}">
								<img class="header-logo-non-sticky opacity-0" alt="Sala do Empreendedor" width="auto" height="110" src="images/front/logo-sala-do-empreendedor.png">
								<img class="header-logo-sticky opacity-0" alt="Sala do Empreendedor" width="auto" height="60" style="margin-top:25px!important; max-height:60px!important" src="images/front/logo-sala-do-empreendedor-vert.png">
							</a>
						</div>
						<div class="header-logo header-logo-sticky-change d-block d-sm-none" style="width: auto; height: 110px;">
							<a href="{{config('url')}}">
								<img class="header-logo-non-sticky" alt="Sala do Empreendedor" width="auto" height="110" src="images/front/logo-sala-do-empreendedor.png">
							</a>
						</div>
					</div>
				</div>
				<div class="header-column justify-content-end">
					<div class="header-row">
						<div class="header-nav header-nav-links header-nav-dropdowns-dark header-nav-light-text order-2 order-lg-1">
							<div class="header-nav-main header-nav-main-effect-2 header-nav-main-font-lg header-nav-main-font-lg-upper-2 header-nav-main-mobile-dark header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
								<nav class="collapse">
									<ul class="nav nav-pills" id="mainNav">
										<li>
											<a data-hash class="nav-link active" href="#home">
												Início
											</a>
										</li>
										<li>
											<a class="nav-link" data-hash data-hash-offset="68" href="#services">O Projeto</a>
										</li>
										<!-- <li>
											<a class="nav-link" data-hash data-hash-offset="68" href="#projects">Serviços</a>
										</li> -->
										<li>
											<a class="nav-link" data-hash data-hash-offset="100" href="#mei">MEI</a>
										</li>
										<li>
											<a class="nav-link" data-hash data-hash-offset="70" href="#municipios">Municipios</a>
										</li>
										<li>
											<a class="nav-link" data-hash data-hash-offset="70" href="#partners">Parceiros</a>
										</li>
									</ul>
								</nav>
							</div>
							<button class="btn header-btn-collapse-nav" data-bs-toggle="collapse" data-bs-target=".header-nav-main nav">
								<i class="fas fa-bars"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>