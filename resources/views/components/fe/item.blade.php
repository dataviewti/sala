@php
    if($item->type === 'link'){
        $params = (object) [
            "icon"=> $item->icon,
            "type"=> $type ?? "featured-box-primary"
        ];
    }
    else{
        $params = (object) [
            "icon"=> $item->icon,
            "type"=> $type ?? "featured-box-quaternary"
        ];
    }

@endphp
<div class="featured-box {{$params->type}} featured-box-effect-2">
    <a class="z-item" href="{{$item->getUrl()}}" target="{{$item->getTarget()}}" data-rm-from-transition="true">
        <div class="box-content box-content-border-bottom">
            <i class="icon-featured material-icons" style="font-size:2.6em!important">
                {{$params->icon}}
            </i>
            <h4 class="font-weight-normal text-5 mt-2"><strong class="font-weight-extra-bold">
                {{$item->name}}
            </strong></h4>
            <!-- <p class="mb-1 mt-2 text-2"></p> -->
        </div>
    </a>
</div>