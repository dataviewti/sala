@php
  $locale = App::getLocale();
@endphp
<div class = 'container-fluid p-0 container-section-header mb-0'>
  <div class = 'row header-top-bar d-flex justify-content-between mb-0'>
    <div class = 'col-11 col-md-10 align-items-center h-100'>
      <div class ='row h-100 pl-md-3'>
        <div class = 'col-6 social-media-blk my-auto pr-0'>
          <a href = '#contato' class="scroll pl-2" >
            <span><i class = 'ico ml-envelope'></i> {{config('site.client.email')}}</span>
          </a>
        </div>
      </div>
    </div>
    <div class = 'col-1 col-md-2 d-flex align-items-center justify-content-end social-media-blk h-100'>
      <a target = '_bkank' href = "{{config('site.social_media.facebook.page')}}"><i class = 'ico ico-facebook hover-effect'></i></a>
      <a target = '_bkank'href = "{{config('site.social_media.instagram.page')}}" class = 'mx-2'><i class = 'ico ico-instagram hover-effect'></i></a>
    </div>
  </div>
  
  <div class = 'row row-fluid fluid'>
    <div class = 'col-12 col-sm-10 mx-auto'>
        @include('fe.layout.menu')
    </div>
  </div>

  <div class = 'row position-relative' style = "z-index:2">
    <div class="col-12 col-sm-10 search-container text-center pt-0 pb-3 mx-auto">
      <div class = 'row'>
        <div class="mx-auto col-11 col-sm-10 text-home d-flex text-center">
          <h3 class='mx-auto my-2 my-sm-3'>ENCONTRE OS MELHORES PRODUTOS E SERVIÇOS, EXCLUSIVAMENTE DE VENDEDORES E AUTÔNOMOS DE SUA CIDADE</h3>
        </div>
      </div>
      <div class = 'row mx-2' id="__ft_search">
        <div class="col-12 col-sm-8 py-2 px-2 px-sm-4 mx-auto">
          <div class="input-group mb-2">
            <input placeholder="busque por produtos ou serviços" type = 'text' class = 'b-none form-control form-control-lg pr-3' name ='ft_search' id = 'ft_search'/>
            <div class="input-group-append" style="background-color:#fff; border-radius:0 .25rem .25rem 0">
              <a id = "trigger_ft_search" href="#__ft_search" class="btn btn-outline-primary b-none d-flex scroll px-0" type="button" data-toggle="tooltip" title="buscar"><span class="ico ml-search m-auto"></span></a>
            </div>
          </div>
          {{-- <small class='placeholder'>busque por produtos, serviços, empresas ou qualquer palavra chave relacionada ao que procura</small> --}}
        </div>
      </div>
    </div>
  </div>
</div>