@php
  //list($img_w, $img_h) = getimagesize(url('fe/images/social-media/share_image.jpg'));
  list($img_w, $img_h) = [800,600];
@endphp

    <meta name="title" content="{{config('site.meta.og.site_name')}}">
    <meta name="description" content="{{ config('site.meta.og.description') }}">
    <meta name="keywords" content="{{ config('site.meta.keywords') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="{{config('site.meta.theme_color')}}">

    <link rel="canonical" href="{{url()->current()}}">
    <meta property="og:type" content="{{config('site.meta.og.type')}}">
    <meta property="og:url" content="{{url()->current()}}">
    <meta property="og:description" content="{{config('site.meta.og.description')}}">
    <meta property="og:title" content="{{config('site.meta.og.title')}}">
    <meta property="og:locale" content="{{config('site.meta.og.locale')}}">
    <meta property="og:site_name" content="{{config('site.meta.og.site_name')}}">

    @if(filled(config('site.social_media.facebook.app_id')))
      <meta property="fb:app_id" content="{{config('site.social_media.facebook.app_id')}}">
    @endif

    <meta property="og:image:width" content="{{$img_w}}">
    <meta property="og:image:height" content="{{$img_h}}">
    <meta property="og:image" content="{{url('/fe/images/social-media/share_image.jpg')}}">

