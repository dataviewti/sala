<div class = 'row'>
  <div class = 'col-12 col-sm-11 colmx-auto pt-4'>
    <div class = 'row d-flex justify-content-between'>
      <div class = 'mx-auto col-10 col-sm-4 d-flex pb-4 pb-sm-0'>
        <a href="{{ url('/') }}" class="my-auto" >
          <img src = "{{asset('/fe/images/mercadolokal-logo.png')}}" class = 'img img-fluid img-responsive' alt = "Logo {{config('site.title')}}">
        </a>
      </div>
        {{-- <div class="col-12 col-sm-12">
          <div class="col-xs-12 col-6 d-flex flex-column text-home b-red ">
            <h3>ENCONTRE OS MELHORES</h3>
            <h3>PRODUTOS E SERVIÇOS</h3>
            <h3>NA SUA CIDADE</h3>
          </div>
        </div> --}}

      <div class = 'col-11 col-sm-5 mx-auto d-flex'>
        <div class="px-2 menu">
          <a class="d-flex my-3 menu-item scroll" href="#o-projeto">
            <div class="landing-icon mr-2 o-projeto"> </div>
            <div>
              <h5 class="mt-1">O Projeto</h5>
              <p>entenda o objetivo, como funciona e como fazer parte do mercadolokal</p>
            </div>
          </a>
          <a class="d-flex my-3  menu-item scroll" href="#cadastro">
            <div class="landing-icon mr-2 quero-vender"> </div>
            <div>
              <h5 class="mt-1">Quero Vender</h5>
              <p>cadastre-se e divulgue seus produtos e serviços de forma totalmente gratuita</p>
            </div>
          </a>
          <a class="d-flex my-3 menu-item scroll" href="#contato">
            <div class="landing-icon mr-2 contato"> </div>
            <div class="mt-1">
              <h5>Contato</h5>
              <p>Tem alguma dúvida, sugestão, gostaria de ser um apoiador? entre em contato</p>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>