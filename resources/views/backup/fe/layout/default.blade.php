<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Cache-control" content="public">
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="robots" content="all" />
    <title>TESTE</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}" defer></script>
  </head>
  <body class="antialiased">
    <div id="app">
      <header>
        @yield('header')
      </header>
      @yield('sections')
      <footer>
        @yield('footer')
      </footer>
    </div>
  </body>
</html>
