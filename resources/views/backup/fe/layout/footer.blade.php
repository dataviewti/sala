<div class = 'row container-fluid footer position-relative m-0 p-0' style = "background-color:#7f7f7f">
  <div class = "bg py-5 position-relative d-flex w-100"></div>
    
    <div class="row col-10 mx-auto d-flex position-relative" style = "min-height:250px; z-index:2">
        <div class="row m-auto w-100">
          <div class="col-12 col-sm-4 d-flex flex-column">
            <a href="{{ url('/') }}" class="d-flex">
              <img src = "{{asset('/fe/images/mercadolokal-logo.png')}}" style = "width:50%" class = 'mx-auto img' alt = "Logo {{config('site.title')}}">
            </a>
            <p class="mt-3 text-center"><small>Encontre os melhores produtos e serviços, exclusivamente de vendedores e autônomos da sua cidade</small><p>
          </div>
          <div class="col-12 col-sm-4 d-flex pt-3">
            <ul class="text-center text-sm-left p-0 mx-auto mt-1 pt-1 mt-sm-4 pt-sm-4">
              <li class= 'mb-1'><h5><strong>Mapa do site</strong></h5></li>
              <li><a class="scroll" href = '#o-projeto'>- O Projeto</a></li>
              <li><a class="scroll" href = '#cadastro'>- Quero Vender</a></li>
              <li><a class="scroll" href = '#contato'>- Contato</a></li>
              <li><a style = "cursor:pointer" class="scroll" data-toggle="modal" data-target="#modal-termos">- Termos e Condições</a></li>
            </ul>
          </div>
          <div class="col-12 col-sm-4 d-flex pt-3">
            <ul class="text-center text-sm-left p-0 mx-auto mt-1 pt-1 mt-sm-4 pt-sm-4">
              <li class= 'mb-1'><h5><strong>Contato</strong></h5></li>
              <li>
                <a class="scroll" href='#contato'><i class = 'ico ml-envelope'></i> {{config('site.client.email')}}</a>              
              </li>
            </ul>
          </div>
        </div>
    </div>
    <div class="row col-12 mx-auto d-flex mt-3 mt-sm-0">
      <div class='row col-12 d-flex justify-content-between mb-2 mx-auto'>
        <p class='m-0 col-12 col-sm-6 text-center text-sm-left'><small>© MercadoLokal 2020</small></p>
        <p class='m-0 col-12 col-sm-6 text-center text-sm-right'><small>Desenvolvido po Dataview TI</small></p>
      </div>
    </div>

    
  </div>
</div>