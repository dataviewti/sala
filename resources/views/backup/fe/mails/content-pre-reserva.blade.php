@extends('mails.html-template')
@section('content')
	<br />
	<h1 class = 'title'>Solicitação de Pré-reserva</h1>
	<br>
	<h5 class="text-center">
	Entraram em contato através do formulário de pré-reserva, segue abaixo os dados informados:<br><br>
</h5>

  <p>Nome: <strong>@{{$data['nome']}}</strong></p>
  <p>email: <strong>@{{$data['email']}}</strong></p>
  <p>telefone: <strong>@{{$data['nome']}}</strong></p>
  <p>Adultos (+8 anos): <strong>@{{$data['n_adultos']}}</strong></p>
  <p>Crianças (até 8 anos): <strong>@{{$data['n_criancas']}}</strong></p>
  <p>Data Chegada: <strong>@{{$data['dtini']}}</strong></p>
  <p>Data Saída: <strong>@{{$data['dtfim']}}</strong></p>
  
@endsection