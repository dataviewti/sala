<div class = 'row section-contato'>
  <div class = 'col-12'>
    <div class = 'col-12 d-flex pt-0 mt-1 mb-2 wow animated fadeInUp' data-wow-duration="1.2s">
      <h1 class = 'm-auto pt-1 pb-2 text-center section-title fg-primary'>Contato</h1>
    </div>

    <div class = 'col-12 col-sm-9 px-5 d-flex mx-auto wow animated fadeInUp' data-wow-delay=".5s" data-wow-duration="1.2s">
      <h4 class = 'text-center m-auto section-subtitle fg-primary fg-dark op-5'>
        Tem alguma dúvida, algum problema, sugestão, gostaria de ser um apoiador? entre em contato conosco, responderemos o mais breve possível
      </h4>
    </div>

    <div class ='row d-flex justify-content-center mt-4' data-wow-duration="1.0s" data-wow-delay=".7s">
      <div class = 'col-10  col-sm-8 mt-2 b-red'>
        <form action = 'contato' class = 'form-theme-classic' method = 'post' id = 'contato-form'>
          <div class ='row'>
            <div class="col-12 col-sm-5">
              <div class="form-group">
                <label for = 'cont_name' class="bmd-label-floating __required"><span class="ico ico-business-card"></span> Informe seu Nome</label>
                <input name = 'cont_name' id = 'cont_name' type = 'text' class = 'form-control form-control-lg' />
              </div>
            </div>
            <div class="col-12 col-sm-4">
              <div class="form-group">
                <label for = 'cont_email' class="bmd-label-floating __required"><span class="ico ml-envelope"></span> Informe seu email</label>
                <input name = 'cont_email' id = 'cont_email' type = 'email' style = 'text-transform:lowercase' class = 'form-control form-control-lg' />
              </div>
            </div>
            <div class="col-12 col-sm-3">
              <div class="form-group">
                <label for = 'cont_phone' class="bmd-label-static"><span class="ico ml-mobile"></span> Telefone para contato</label>
                <input name = 'cont_phone' id = 'cont_phone' type = 'tel' class='form-control form-control-lg' />
              </div>
            </div>
          </div>
          <div class = 'row'>
            <div class="col-12 col-sm-9">
              <div class="form-group" style = 'position:relative!important'>
                <label for = 'cont_message' class="bmd-label-static __required"><i class="ico ico-edit"></i> Mensagem</label>
                <textarea name = 'cont_message' id = 'cont_message' 
                style = 'height:80px;' class = 'form-control form-control-lg'></textarea>
              </div>
            </div>
            <div class="col-12 col-sm-3 d-flex align-content-end my-3 my-sm-0">
              <div class = 'my-auto flex-fill'>
                <button type = 'submit' class = 'btn-lg btn btn-outline-primary my-auto mt-2'>
                  <i class = ' ico ml-envelope'></i> Enviar Mensagem
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  
  </div>
</div>

