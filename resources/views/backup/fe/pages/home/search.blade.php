@php
  use Jenssegers\Agent\Agent;
  $agent = new Agent();
  $gast_url = ($agent->is('iPhone') || $agent->is('OS X')) ? "https://apps.apple.com/br/app/tonolucro-delivery-de-tudo/id1034307004" : "https://play.google.com/store/apps/details?id=br.com.tonolucro.delivery";
@endphp
<div class = 'row py-0 py-sm-4 px-4 position-relative' style="background-color:#fff!important;">
  <div class = "edge-mask-fit"></div>
  <div class="col-12 mb-5">
    <div class = 'row'>
      <div class="col-12 col-sm-3 side-search">
        @if(!$agent->isDesktop())
          <div class="pos-f-t">
            <nav class="navbar  p-0" style="background-color: transparent;">
              <button class="navbar-toggler py-2 btn-block btn btn-outline-info" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="ico ico-filter"></span> Filtrar Pesquisa
              </button>
            </nav>
            <div class="collapse" id="navbarToggleExternalContent">
              <div class = 'row mt-3'>
                <div class="col-12 slim-select-container">
                  <div class="form-group">
                    <label for = 'ft_category' class = 'bmd-label-static mb-2'><i class = 'ico ico-filter'></i> Categoria</label>
                    <select id="ft_category" name = "ft_category">
                    </select>          
                  </div>    
                </div>
                <div class="col-12 slim-select-container">
                  <div class="form-group">
                    <label for = 'ft_subcategories' class = 'bmd-label-static mb-2'><i class = 'ico ico-filter'></i> Subcategorias</label>
                    <select id="ft_subcategories" name = "ft_subcategories" multiple>
                    </select>          
                  </div>    
                </div>
              </div>
            </div>
          </div>
        @else
        <div class = 'row'>
          <div class="col-12 slim-select-container">
            <div class="form-group">
              <label for = 'ft_category' class = 'bmd-label-static mb-2'><i class = 'ico ico-filter'></i> Categoria</label>
              <select id="ft_category" name = "ft_category">
              </select>          
            </div>    
          </div>
          <div class="col-12 slim-select-container">
            <div class="form-group">
              <label for = 'ft_subcategories' class = 'bmd-label-static mb-2'><i class = 'ico ico-filter'></i> Subcategorias</label>
              <select id="ft_subcategories" name = "ft_subcategories" multiple>
              </select>          
            </div>    
          </div>
        </div>
        @endif

      </div>
      <div class="col-12 col-sm-7 px-0 pr-sm-2 pt-4 pt-sm-0">
        <table class="no-stripes m-0" id="provider-fe-table" cellspacing="0">
          <thead style = "display:none">
            <th class = "__dt_block">block</th>
            <th class = "__dt_name">name</th>
            <th class = "__dt_categoria-principal">Categoria Principal</th>
            <th class = "__dt_subcategories">subcategories</th>
            <th class = "__dt_delivery">delivery</th>
          </thead>
        </table>
      </div>
      <div class="col-12 col-sm-2 p-0 pt-4 pt-sm-1">
        <a target="_blank" href="{{ $gast_url }}" class="d-flex" >
          <img src = "{{asset('/fe/images/ads/circuito-gastronomico.jpg')}}" class = 'mx-auto img img-fluid img-responsive' alt = "circuito gastronômico online de Gurupi - clique para baixar o app">
        </a>
      </div>
    </div>
  </div>
</div>