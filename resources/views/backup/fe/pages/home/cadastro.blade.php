<div class = 'row section-cadastro pb-5' style = "background-color:#fff!important; position:relative">
  <div class = "bg py-5 position-relative d-flex w-100"></div>
  <div class = 'col-12'>
    {{-- <div class = "edge-mask" style = "margin-top:-90px"></div> --}}
    <div class = 'col-12 d-flex pt-2 mt-2  mt-2 mb-2 wow animated fadeIn' scroll-adjust="35" data-wow-duration="1.2s">
      <h1 class = 'm-auto py-2 text-center section-title fg-primary' style="">Faça seu cadastro</h1>
    </div>

    <div class = 'col-12 col-sm-8 px-3 px-sm-5 d-flex mx-auto wow animated fadeIn' data-wow-duration="1.5s">
      <h4 class = 'text-center m-auto section-subtitle fg-primary fg-dark op-5'>
        Informe os dados abaixo para cadastrar seus produtos e/ou serviços, seu cadastro ficará em análise e será liberado em breve
      </h4>
    </div>
      
    <form action = '/provider/create' id='provider-form' method = 'post' class="mt-4">
      <div class ='row'>
        <div class = "col-11 mx-auto px-3 px-sm-0">
          <div class = 'row pt-2'>
            <div class="col-12 col-sm-7" style = 'border-right:1px #e1f0ee solid'>
              <div class = 'row'>
                <div class="col-12 col-sm-4">
                  <div class="form-group">
                    <label for = 'cpf_cnpj' class="bmd-label-floating __required"><span class="ico ico-business-card"></span> CPF / CNPJ</label>
                    <input name = 'cpf_cnpj' id="cpf_cnpj" type = 'text' class = 'form-control form-control-lg' />
                  </div>
                </div>
                <div class="col-12 col-sm-8">
                  <div class="form-group">
                    <label for = 'name' class="bmd-label-floating __required"><span class="ico ico-business-card"></span> Seu nome / Nome da sua empresa ou negócio</label>
                    <input name = 'name' type = 'text' class = 'form-control form-control-lg' />
                  </div>
                </div>
              </div>


              <div class = 'row'>
                <div class="col-12 col-sm-4">
                  <div class="form-group">
                    <label for='phone' class="bmd-label-floating"><span class="ico ml-mobile"></span> Telefone</label>
                    <input type="text" id='phone' name='phone' class = 'form-control form-control-lg input-lg' />
                  </div>
                </div>
                <div class="col-12 col-sm-4">
                  <div class="form-group">
                    <label for = 'isWhatsapp' class="bmd-label-floating __required"><span class="ico ico-whatsapp"></span> O telefone é whatsapp?</label>
                    <br>
                    <div class="text-center mt-3 aanjulena-container">
                      <span class="my-auto mt-2 aanjulena-no">Não</span>
                      <button type="button" class="btn btn-sm aanjulena-btn-toggle active"
                          data-toggle="button" aria-pressed="true" 
                          data-default-state='false' autocomplete="off" name = 'isWhatsapp' id = 'isWhatsapp'
                          >
                        <div class="handle"></div>
                      </button>
                      <span class="my-auto mt-2 aanjulena-yes">Sim</span>
                      <input type = 'hidden' name = '__isWhatsapp' id = '__isWhatsapp' />
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-4">
                  <div class="form-group">
                    <label for = 'delivery' class="bmd-label-floating __required"><span class="ico ico-delivery"></span> Faz entrega  (delivery)</label>
                    <br>
                    <div class="text-center mt-3 aanjulena-container">
                      <span class="my-auto mt-2 aanjulena-no">Não</span>
                      <button type="button" class="btn btn-sm aanjulena-btn-toggle active"
                          data-toggle="button" aria-pressed="true" 
                          data-default-state='false' autocomplete="off" name = 'delivery' id = 'delivery'
                          >
                        <div class="handle"></div>
                      </button>
                      <span class="my-auto mt-2 aanjulena-yes">Sim</span>
                      <input type = 'hidden' name = '__delivery' id = '__delivery' />
                    </div>
                  </div>
                </div>
              </div>

              <div class = 'row'>
                <div class="col-12 col-sm-7">
                  <div class="form-group">
                    <label for='email' class="bmd-label-floating"><span class="ico ico-email"></span> email</label>
                    <input type="email" id='email' name='email' class = 'form-control input-lg form-control-lg'/>
                  </div>
                </div>
                <div class="col-12 col-sm-5">
                  <div class="form-group">
                    <label for='instagram' class="bmd-label-floating"><span class="ico ico-instagram"></span> Qual é seu Instagram?</label>
                    <input type="text" id='instagram' name='instagram' placeholder="Ex: @meuinstagram" class = 'form-control input-lg form-control-lg' />
                  </div>
                </div>
              </div>   
            </div>
            
            <div class=" col-12 col-sm-5">
              <div class = 'row'>
                <div class="col-12">
                  <div class="form-group" style="height:auto!important">
                    <label for='description' class="bmd-label-floating"><span class="ico ico-list"></span> Descreva da melhor forma seus produtos/serviços</label>
                    <textarea id='description' maxlength="200" name='description' class='form-control input-lg form-control-lg' style="height:120px" placeholder="Por exemplo: Vendo espetinhos de carne,frango, linguiça, simples e completo, jantinha com feijão tropeiro, torresmo etc..."></textarea>
                  </div>
                </div>
              </div>
              
              <div class = 'row'>
                <div class="col-12 slim-select-container">
                  <div class="form-group">
                    <label for = 'name' class="bmd-label-floating __required mb-3">
                      Escolha a categoria principal do seu negócio
                    </label>
                    <select id="category" name = "category" class = 'form-control input-lg form-control-lg'>
                    </select>          
                  </div>
                </div>

                {{-- <div class="col-12 slim-select-container">
                  <div class="form-group">
                    <label for = 'ft_category' class = 'bmd-label-static mb-2'><i class = 'ico ico-filter'></i> Categoria</label>
                    <select id="ft_category" name = "ft_category">
                    </select>          
                  </div>    
                </div> --}}

              </div>

              <div class = 'row'>
                <div class="col-12 col-sm-12">
                  <div class="form-group form-group-ss form-group-ss-multi" style="height:auto">
                    <label for = 'name' class="bmd-label-floating __required mb-3">
                      Selecione até 4 subcategorias 
                    </label>
                    <select id="subcategories" name = "subcategories" multiple class = 'form-control input-lg form-control-lg'>
                      <option data-placeholder="true"></option>
                    </select>          
                    <input type = "hidden" name = "__subcategories" id="__subcategories" />
                  </div>
                </div>
              </div>

              <div class = 'row'>
                <div class="col-12 col-sm-7">
                  <div class="form-group" style="height:40px!important">
                    <div class="mt-2 aanjulena-container">
                      <span class="my-auto mt-1 aanjulena-no">
                        <u style="color:#17a2b8"><a style="cursor:pointer; color:#17a2b8" data-toggle="modal" data-target="#modal-termos" class="mr-2">Aceito os termos e condições</a></u></span>
                        <button type="button" class="btn btn-xs aanjulena-btn-toggle active"
                          data-toggle="button" aria-pressed="true" 
                          data-default-state='false' autocomplete="off" name = 'agree' id = 'agree'
                          >
                        <div class="handle"></div>
                      </button>
                      <input type = 'hidden' name = '__agree' id = '__agree' />
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-5">
                  <div class = 'align-items-end d-flex flex-column'>
                    <button type = 'submit' class = 'mt-auto btn btn-lg btn-success'><i class = ' ico ico-save'></i> Cadastrar</button>
                  </div>
                </div>
              </div>
            </div>  
          </div>
        </div>
      </div>
      <input type = 'hidden' name = 'fromsite' value='true' />
    </form>
  </div>
</div>