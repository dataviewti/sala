<div class="modal fade" id="modal-termos" role="dialog" aria-labelledby="odal-termos" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">MercadoLokal - Termos e Condições</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style = "height:300px; overflow-y:scroll">
        <div>
          <p class= "my-2 text-justify">O MercadoLokal não é fornecedor de quaisquer produtos ou serviços anunciados no site. O MercadoLokal presta unica e exclusivamente um serviço de oferta de uma plataforma na internet que fornece espaços para que usuários (potenciais vendedores), anunciem, oferecendo à venda, os seus próprios produtos e serviços para que eventuais interessados na compra dos itens, os usuários (potenciais compradores), possam negociar de forma direta e exclusivamente entre si.</p>

          <p class= "my-2 text-justify">Os usuários (potenciais vendedores) somente poderão anunciar produtos ou serviços que possam vender e que tenham em estoque, estabelecendo diretamente os termos do anúncio e todas as suas características, e que não sejam expressamente proibidos ou violem a legislação vigente.</p>

          <p class= "my-2 text-justify">O MercadoLokal, poderá a qualquer tempo, em razão de violação à legislação em vigor ou aos Termos e condições gerais de uso do MercadoLokal, conforme a situação, poderá, sem prejuízo de outras medidas, recusar qualquer solicitação de cadastro, advertir, suspender, temporária ou definitivamente, a conta de um usuário.</p>

          <p class= "my-2 text-justify">Ao confirmar este cadastro, o usuário assume total responsabilidade sobre a veracidade dos dados informados, bem como de todas as implicações decorrentes do processo de compra e venda e da disponibilização de forma pública de dados de contato como: telefone, email e endereço de redes sociais.</p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>