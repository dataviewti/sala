<div class = 'col-12 d-flex pt-4 mb-0 mt-5 mb-2 wow animated fadeIn' scroll-adjust="35" data-wow-duration="1.2s">
    <div class = 'separator-secondary mx-auto'>
      <h1 class = 'm-auto py-2 text-center pb-4'>Sustentabilidade</h1>
    </div>
</div>

<div class="row">
  <div class = 'col-12 col-sm-10 px-5 d-flex py-2 mx-auto wow animated fadeIn' data-wow-duration="1.5s">
    <h4 class = 'text-center m-auto subtitulo'>
  A utilização de briquetes em relação aos combustíveis tradicionais, apresenta uma enorme lista de vantagens
    </h4>
  </div>
</div>
  
<div class ='row mx-0 mb-5 mt-md-5 b-green'>
  <div class = 'col-12 col-md-7 d-flex pr-md-5 mt-3 mt-sm-0'>
    <ul class="b-red vantagens">
      <li>Poder calorífico 2,5X maior que o da lenha comum</li>
      <li>Regularidade Térmica</li>
      <li>Não exala cheiro e não deixa gosto</li>
      <li>A forma de iniciar a lenha é a mesma da lenha comum</li>
      <li>A combustão do produto é limpa</li>
      <li>Maior temperatura da Chama</li>
      <li>Alta combustão e grande poder calorífico</li>
      <li>Rápida resposta de temperatura e maior estabilidade</li>
      <li>Baixa manutenção de fornalhas e grelhas</li>
      <li>Não danifica a fornalha no manuseio de abastecimento</li>
      <li>Devido a baixa umidade produz menos fumaça, cinza e fuligem em relação à lenha</li>
    </ul>
  </div>
</div>
