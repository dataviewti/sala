<div class = 'row section-o-projeto'>
  <div class = 'col-12 bg'>
    <div class = 'col-12 d-flex pt-2 mt-2 mb-2 wow animated fadeIn' scroll-adjust="35" data-wow-duration="1.2s">
      <h1 class = 'm-auto pt-2 text-center section-title fg-third fg-light'>O projeto mercadolokal</h1>
    </div>

    <div class = 'col-12 col-sm-10 px-5 d-flex mx-auto wow animated fadeIn' data-wow-duration="1.5s">
      <h4 class = 'text-center m-auto section-subtitle fg-third'>
        Entenda o objetivo e como funciona o mercadolokal
      </h4>
    </div>
      
    <div class ='row my-3 my-sm-5 py-3 px-3 pl-sm-3 justify-content-start offset-sm-1'>
      <div class = 'col-12 col-sm-6 d-flex order-md-1 flex-column'>
          <h5 class="wow animated fadeInLeft pl-2 pl-sm-4 mb-2 mb-sm-4" data-wow-duration="1s">Em razão da pandemia do <b>COVID-19</b> que assola o mundo, muitas pessoas/empresas se encontram em uma difícil situação, das grandes aos vendedores informais, prestadores de serviços, feirantes, etc. Todos estão sofrendo as  consequências.</h5>
          <br />
          <h5 class="wow animated fadeInLeft fg-secondary-color-dark mt-4 pl-2 pl-sm-4" data-wow-duration="1s" data-wow-delay=".5s">O objetivo desta plataforma é fornecer um canal de divulgação <b>gratuito</b> para produtos e serviços, com foco exclusivo no mercado local de <b>GURUPI</b>.</h5>
          <h5 class="wow animated fadeInLeft fg-secondary-color-dark mt-4 pl-2 pl-sm-4" data-wow-duration="1s" data-wow-delay=".7s">Muitos já utilizam outras plataformas ou mesmo redes sociais para divulgação, mas nem sempre é fácil ser encontrado em meio a um mar de informações, por isso a ideia de fazer algo mais simples, direto e exclusivo para <b>GURUPI</b>.</h5>
      </div>
    </div>
  </div>
</div>

