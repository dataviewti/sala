<div class = 'row section-apoiadores'>
  <div class = 'col-12 mb-5 pb-5'>
    <div class = 'col-12 d-flex pt-2 pt-sm-4 mt-2  mt-sm-4 mb-2 wow animated fadeInUp' data-wow-duration="1.2s">
      <h1 class = 'm-auto py-2 text-center section-title fg-primary'>Apoiadores</h1>
    </div>

    <div class = 'col-12 col-sm-8 px-5 d-flex mx-auto wow animated fadeInUp' data-wow-delay=".5s" data-wow-duration="1.1s">
      <h4 class = 'text-center m-auto section-subtitle fg-primary fg-dark op-5'>
        conheça as empresas e instituições que apoiam o projeto mercadolokal
      </h4>
    </div>
      
    <div class ='row py-2 py-sm-5 valley-effect-container'>
      <div class = 'col-12 col-sm-9 mx-auto py-2'>

        <div class ='row justify-content-center px-3'>
          <div class = 'col-9 col-sm-3 my-4 wow animated fadeIn' data-wow-duration="2s">
            <div class="apoiador-block p-2">
              <div class="block-img" style="background-image:url('/fe/images/apoiadores/dataview.png');"></div>
              <h5 class="text-center fg-primary fg-dark mt-2 op-8">Dataview TI</h5>
              <p class="text-center m-0">
                {{-- <a href="#" target="_self" class='mx-1' ><span class="ico ico-facebook"></span></a>
                <a href="#" target="_self" class='mx-1'> <span class="ico ico-instagram"></span></a> --}}
              </p>
            </div>
          </div>
          <div class = 'col-9 col-sm-3 my-4 wow animated fadeIn' data-wow-duration="2s">
            <div class="apoiador-block p-2">
              <div class="block-img" style="background-image:url('/fe/images/apoiadores/sebrae.png');"></div>
              <h5 class="text-center fg-primary fg-dark mt-2 op-8">Sebrae / Gurupi</h5>
              <p class="text-center m-0">
                <a href="https://www.facebook.com/Sebraetocantins" target="_blank" class='mx-1' >
                  <span class="ico ico-facebook"></span>
                </a>
                <a href="https://www.instagram.com/sebraeto" target="_blank" class='mx-1'>
                  <span class="ico ico-instagram"></span>
                </a>
              </p>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

