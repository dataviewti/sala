<div class = 'row section-contato'>
  <div class = 'col-12 bg' style = "padding-bottom:50px">
    <div class = 'col-12 d-flex pt-0 mt-1 mb-2 wow animated fadeInUp' data-wow-duration="1.2s">
      <h1 class = 'm-auto pt-1 text-center section-title fg-primary'>Contato</h1>
    </div>

    <div class = 'col-12 col-sm-8 px-5 px-sm-2 d-flex mx-auto wow animated fadeInUp' data-wow-delay=".5s" data-wow-duration="1.2s">
      <h4 class = 'text-center m-auto section-subtitle fg-primary fg-dark op-5'>
        Tem alguma dúvida, algum problema, sugestão, gostaria de ser um apoiador? entre em contato conosco, responderemos o mais breve possível
      </h4>
    </div>

    <form action = 'contato' class = 'form-theme-classic' method = 'post' id = 'contato-form'>
      <div class ='row mx-3 mx-md-6 mt-3 d-flex justify-content-center'>
        <div class = "col-12 offset-sm-6 col-md-4 fadeInRight wow animated" data-wow-delay=".5s">
          <div class = 'row'>
            <div class="col-12">
              <div class="form-group mb-4 mb-sm-3">
                <label for = 'cont_name' class="bmd-label-floating __required"><span class="ico ico-business-card"></span> Informe seu Nome</label>
                <input name = 'cont_name' id = 'cont_name' type = 'text' class = 'form-control form-control-lg' />
              </div>
            </div>
          </div>
          <div class = 'row'>
            <div class="col-12">
              <div class="form-group mb-4 mb-sm-3">
                <label for = 'cont_email' class="bmd-label-floating __required"><span class="ico ml-envelope"></span> Informe seu email</label>
                <input name = 'cont_email' id = 'cont_email' type = 'email' style = 'text-transform:lowercase' class = 'form-control form-control-lg' />
              </div>
            </div>
          </div>
          <div class = 'row'>
            <div class="col-12">
              <div class="form-group mb-4 mb-sm-3">
                <label for = 'cont_phone' class="bmd-label-static"><span class="ico ml-mobile"></span> Telefone para contato</label>
                <input name = 'cont_phone' id = 'cont_phone' type = 'tel' class='form-control form-control-lg' />
              </div>
            </div>
          </div>
          <div class = 'row'>
            <div class="col-12">
              <div class="form-group mb-4 mb-sm-3">
                <label for = 'cont_message' class="bmd-label-static __required"><i class="ico ico-edit"></i> Mensagem</label>
                <textarea name = 'cont_message' id = 'cont_message' 
                style = 'height:75px;' class = 'form-control form-control-lg'></textarea>
              </div>
            </div>
          </div>
          <div class = 'row'>
            <div class="col-12 mt-2">
              <div class=" d-flex mb-4 justify-content-end">
                <button type = 'submit' class = 'btn btn-outline-info mr-0'>
                  <i class = ' ico ml-envelope'></i> Enviar Mensagem
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

