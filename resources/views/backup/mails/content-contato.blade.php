@extends('mails.html-template')
@section('content')
	<br />
	<h1 class = 'title'>Entraram em Contato</h1>
	<br>
	<h5 class="text-center">
	Entraram em contato através do formulário de contato, segue abaixo os dados informados:<br><br>
</h5>

  <p>Nome: <strong>@{{$data['cont_name']}}</strong></p>
  <p>email: <strong>@{{$data['cont_email']}}</strong></p>
  <p>telefone: <strong>@{{$data['cont_phone']}}</strong></p>
  <p>Mensagem: <strong>@{{$data['cont_message']}}</strong></p>
  
@endsection