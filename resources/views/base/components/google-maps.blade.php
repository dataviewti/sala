<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAREVkQNq6uams5Otx9f42ajRySvHNiSiI"></script>
<script>
    function initializeGoogleMaps() {
        // Map Markers
        var mapMarkers = [{
            address: "Avenida Mato Grosso, 1576",
            html: "<strong>New York Office</strong><br>New York, NY 10017",
            icon: {
                image: "img/pin.png",
                iconsize: [26, 46],
                iconanchor: [12, 46]
            },
            popup: true
        }];

        var initLatitude = -11.737313583702068;
        var initLongitude = -49.031672044877425;

        // Map Extended Settings
        var mapSettings = {
            controls: {
                draggable: (($.browser.mobile) ? false : true),
                panControl: true,
                zoomControl: true,
                mapTypeControl: true,
                scaleControl: true,
                streetViewControl: true,
                overviewMapControl: true
            },
            scrollwheel: false,
            markers: mapMarkers,
            latitude: initLatitude,
            longitude: initLongitude,
            zoom: 11
        };

        var map = $('#googlemaps').gMap(mapSettings);
    }

    // Initialize Google Maps when element enter on browser view
    theme.fn.intObs( '.google-map', 'initializeGoogleMaps()', {} );

    // Map text-center At
    var mapCenterAt = function(options, e) {
        e.preventDefault();
        $('#googlemaps').gMap("centerAt", options);
    }
</script>
