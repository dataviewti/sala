<?php

namespace App\View\Components\fe;

use Illuminate\View\Component;

class item extends Component
{
    public $item;
    public $type;

    public function __construct($type=null, $item) {
        $this->type = $type;
        $this->item = $item;
    }

    public function render()
    {
        return view('components.fe.item');
    }
}
