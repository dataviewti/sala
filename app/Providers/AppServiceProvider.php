<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\City;
use App\Observers\CityObserver;
use App\Models\Contact;
use App\Observers\ContactObserver;
use App\Models\Item;
use App\Observers\ItemObserver;
use App\Models\File;
use App\Observers\FileObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(){
        City::observe(CityObserver::class);
        Contact::observe(ContactObserver::class);
        Item::observe(ItemObserver::class);
        File::observe(FileObserver::class);
    }
}
