<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\City;
use App\Models\Service;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Traits\Moldable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, Moldable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'city_id',
        'type_id',
        'permissions'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function city () {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function services () {
        return $this->hasMany(Service::class,'id');
    }
    
    public function getPermissions(){
        $perm = DB::table('user_services')
        ->join('services','services.id','=','user_services.service_id')
        ->select('name','create','update','delete','view','ico','description',
        'user_services.id','service_id','alias','user_services.order')
        ->orderBy('user_services.order')
        ->where('user_services.user_id',$this->id)
        ->get();
        
        return collect($perm)
            ->map(function ($d) {
                return [
                    "id" => $d->id,
                    "service_id" => $d->service_id,
                    "name"=>$d->name,
                    "create" => (bool) $d->create,
                    "update" => (bool) $d->update,
                    "delete" => (bool) $d->delete,
                    "view" => (bool) $d->view,
                    "ico" => $d->ico,
                    "alias"=>$d->alias,
                    "order"=>$d->order,
                    "description" =>$d->description
                ];
            });
    }
}
