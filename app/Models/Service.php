<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

class Service extends BaseModel
{
    protected $fillable = [
        'name',
        'alias',
        'ico',
        'description',
        'order',
    ];

    public function users () {
        return $this->belongsToMany(User::class, 'user_id');
    }
}
