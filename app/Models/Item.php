<?php

namespace App\Models;

use App\Models\File;

class Item extends BaseModel
{
    protected $fillable = [
        'city_id',
        'name',
        'type',
        'filename',
        'description',
        'url',
        'icon',
        'new_window',
        'broken_report',
    ];

    protected $casts = [
        'new_window' => 'bool',
        'broken_report' => 'bool',
    ];

    public function city () {
        return $this->belongsTo(City::class, 'city_id','id');
    }

    public function categories () {
        return $this->belongsToMany(Category::class)
        ->withPivot('main');
    }

    public function mainCategory () {
        return $this->belongsToMany(Category::class)
        ->wherePivot('main',true);
    }

    public function file (){
        return $this->belongsTo(File::class, 'file_id','id');
    }

    public function getUrl (){
        if(filled($this->file)){
            return  config('app.url')
            ."/documents/download/"
            .($this->file->name);
        }
        else
            return $this->url;
    }

    public function getTarget (){
        return $this->new_window ? "_blank" : '_self';
    }    
}
