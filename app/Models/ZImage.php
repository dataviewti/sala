<?php

namespace App\Models;

class ZImage extends BaseModel {
    protected $fillable = [
        'name',
        'file_path',
        'counter' 
    ];

    public function counterIncrease(){
        $this->counter++;
        $this->saveQuietly();
    }
}
