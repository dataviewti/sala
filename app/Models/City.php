<?php

namespace App\Models;
use Illuminate\Support\Str;

class City extends BaseModel{
    protected $fillable = [
        'name',
        'short_name',
        'address',
        'address2',
        'number',
        'zip_code',
        'region',
        'phone',
        'mobile',
        'email',
        'lat',
        'lng',
        'google_maps',
        'google_schedule',
        'office_hours',
        'contact_id',
        'banner_id',
        'status'
    ];

    protected $dates = [
    ];
      
    protected $casts = [
        'phone' => 'array',
        'mobile' => 'array',
        'status' => 'boolean'
    ];

    protected $appends = [
        'bannerBase64'
    ];
        public function contacts (){
        return $this->hasMany(Contact::class, 'city_id');
    }
    
    public function file (){
        return $this->belongsTo(File::class, 'banner_id','id');
    }


    public function mainContact (){
        return $this->belongsTo(Contact::class, 'contact_id');
    }

    public function fullAddress (){
        $r = [
            $this->address,
            filled($this->address2) ? ", {$this->address2}" : "",
            filled($this->number) ? ", {$this->numer}" : "",
            "<br /> {$this->name} - {$this->region}"
        ];

        return collect($r)->join('');
    }

    public function items (){
        return $this->hasMany(Item::class, 'city_id');
    }

    public function getSlug(){
        return optional($this)->short_name ?? Str::slug($this->name, '-');
    }

    public function getBannerBase64Attribute(){
        return filled($this->file) ? $this->file->getDataUrl() : null;
    }
}
