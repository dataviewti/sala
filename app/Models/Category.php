<?php

namespace App\Models;

class Category extends BaseModel
{
    protected $fillable = [
        'city_id',
        'category_id',
        'name',
        'description',
        'order',
        'data'
    ];

    protected $casts = [
        'data' => 'array',
    ];
    
    public function category(){
        return $this->belongsTo(Category::class, 'category_id')
        ->whereNull('category_id');
    }
  
    public function maincategory(){
        return $this->belongsTo(Category::class, 'category_id');
    }
  
    public function subcategories(){
        return $this->hasMany(Category::class, 'category_id');
    }
  
    public function childCategories(){
        return $this->subcategories()->with('childCategories');
    }    

    public function items(){
        return $this->belongsToMany(Item::class)
            ->withPivot('main');
    }    
}
