<?php

namespace App\Models;

use City;

class Contact extends BaseModel
{
    protected $fillable = [
        'city_id',
        'name',
        'phone',
        'whatsapp_message',
        'email',
    ];

    protected $casts = [
        'phone' => 'array',
    ];

    public function city () {
        return $this->belongsTo(City::class, 'city_id','contact_id');
    }
}
