<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Relations\Pivot;

class CategoryItem extends Pivot
{
    protected $fillable = [
        'user_id',
        'service_id',
        'main'
    ];

    protected $casts = [
        'main' => 'bool',
    ];
}
