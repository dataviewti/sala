<?php

namespace App\Models;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Mime\MimeTypes;
use Exception;


class File extends BaseModel {
    protected $fillable = [
        'name',
        'file_path',
        'data',
        'counter',
    ];

    protected $appends = [
        'url'
    ];

    public function counterIncrease(){
        $this->counter++;
        $this->saveQuietly();
    }

    // public function getUrlAttribute (){
    //     return Storage::path($this->file_path);
    // }

    public function getUrlAttribute (){
        return Storage::url($this->file_path);
    }

    public function getDataUrl (){
        $file = Storage::disk('public')->get($this->file_path);
        $mimeTypes = new MimeTypes();
        $mime = $mimeTypes->guessMimeType(public_path($this->getUrlAttribute()));
        try {
            return "data:{$mime};base64,".base64_encode($file);
            // return (string) Image::make($file)->encode('data-url');
        }
        catch(Exception $e) {
            return null;
        }
    }    
}
