<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Relations\Pivot;

class UserServices extends Pivot
{
    protected $fillable = [
        'user_id',
        'service_id',
        'create',
        'update',
        'delete',
        'view',
    ];

    protected $casts = [
        'create' => 'boolean'
    ];  
}



// namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

// class UserServices extends Model
// {
//     protected $fillable = [
//         'user_id',
//         'service_id',
//         'create',
//         'update',
//         'delete',
//         'view',
//     ];

//     protected $casts = [
//         'create' => 'boolean'
//     ];    
// }
