<?php

namespace App\Traits;

trait Moldable {

    protected $_appends = ['temp'=>null];

    static function defaultRules($params=[]) {
      $def = ["action"=>"create", "data"=>[], "prefix"=>null];

      $arr = $params[1] ?? [];
      if(is_array($params[0]))  
        $def = collect($def)->merge($params[0])->all();
      else
        $def['action'] = $params[0];

      //extract common
      $ret = array_filter($arr,function($v){
        return !is_array($v);
      });

      if(array_key_exists($def['action'],$arr))
        $ret = collect($ret)->merge($arr[$def['action']])->all();

      if(filled($def['prefix']))
        foreach($ret as $k=>$v){
          $ret["{$def['prefix']}.{$k}"] = $v;
          unset($ret[$k]);
        }
      return $ret ?? [];
    }
    
    static function defaultMessages($params=[]) {   
      return Moldable::defaultRules($params);
    }

    public static function getConstants() {
      $oClass = new \ReflectionClass(get_called_class());
      return $oClass->getConstants();
    }

    public static function getTranslations() {
      return [];
    }

    public function setAppend($index='',$value){
      data_set($this->_appends,$index,$value);
      return $this;
    }

    public function clearAppends(){
      $this->_appends = ['temp'=>null];
    }

    public function getAppend($index=null) {
      return filled($index) ? data_get($this->_appends,$index) : $this->_appends;
    }

}