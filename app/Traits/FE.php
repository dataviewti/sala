<?php

namespace App\Traits;

trait FE
{
  public static function getPhone($p,$message="Olá, como podemos ajudá-lo?") {
    if(filled($p)){
      $p = (object) $p;
      $url= $p->whatsapp ? FE::getWhatsppLink($p->value,$message) : null;

      return (object) collect($p)
      ->merge([
        "url" =>  $url,
        "cleanValue" =>  preg_replace('~\D~', '', $p->value),
        "a"=> filled($url) 
          ? sprintf("<a href='{%s}'>{%s}</a>",$url,$p->value)
          : null
      ])->all();
    }

    return (object) [
      "value"=>null
    ];
  }

  public static function getWhatsppLink($number,$message='',$prefix=55) {
    $fullnumber= preg_replace('~\D~', '', $number);
    $encodedmessage = urlencode($message);
    return sprintf("https://wa.me/%s?text=%s'",$prefix.$fullnumber,$encodedmessage);
  }

}