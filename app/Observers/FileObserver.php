<?php

namespace App\Observers;

use Illuminate\Support\Facades\Storage;
use App\Models\File;

class FileObserver
{
    public function creating(File $obj) {
    }

    public function updating(File $obj) {
    }

    public function deleted(File $obj) {
        // $fp = Storage::path($obj->file_path);
        Storage::delete($obj->file_path);
    }

}
