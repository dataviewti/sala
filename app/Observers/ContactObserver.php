<?php

namespace App\Observers;

use App\Models\Contact;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;

class ContactObserver
{

    public function created(Contact $obj) {
        $this->manageUser($obj);
    }

    public function updated(Contact $obj) {
        $this->manageUser($obj);
    }

    public function manageUser(Contact $obj){


        $user = $obj->getAppend('__createUser');
        if($user === true){
            //checa se o email já existe
            if(!User::where('email',$obj->email)->exists()){
                $_user = User::create([
                    "city_id"=>$obj->city_id,
                    "type_id"=>2, //municipio
                    'name' => $obj->name,
                    'email' => $obj->email,
                    'password' => Hash::make('teste123'),
                ]);
        
                event(new Registered($user));
            }
         }
    }    
}
