<?php

namespace App\Observers;

use App\Models\City;
use App\Models\Contact;
use App\Models\File as ZFile;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class CityObserver
{

    public function created(City $obj) {
        $this->manageContact($obj)
        ->manageFile($obj);
    }

    public function updated(City $obj) {
        $this->manageContact($obj)
            ->manageFile($obj);
    }
    
    public function manageContact(City $obj){


        $resp = $obj->getAppend('__resp') ?? [];



       if(filled($resp)){

            $resp = (object) $resp;
            if(is_string($resp->name)) {
                $contact = new Contact(collect($resp)->merge([
                    "city_id"=>$obj->id
                ])->all());

                $contact
                    ->setAppend('__createUser',optional($resp)->createUser);
            
                $contact->save();
            }
            else {

                $cdata = (object) $resp->name;
                $contact = Contact::where("id",$cdata->id)->first();

                $contact->update([
                    "phone"=> $resp->phone,
                    "whatsapp_message"=> $resp->whatsapp_message,
                    "email"=> $resp->email
                ]);

                $contact
                    ->setAppend('__createUser',optional($resp)->createUser)
                    ->touch();
            }

            $obj->mainContact()
                ->associate($contact);
            $obj->saveQuietly();
        }

        return $this;
    }

    public function manageFile(City $obj){
        $file = $obj->getAppend('__file');

        if(filled($file)){
            $f = (object) $file;
            // remove se existe outro arquivo previo
            //atualiza imagem para Intervention
            if(filled($f->file)){


                $img = Image::make($f->file)
                ->fit(1900, 600);
            
            
                $fext = $f->file->guessExtension();
                $fname = Str::slug($obj->name, '-')."-{$obj->id}.{$fext}";
                
                $path = Storage::path('public/zorro/images/'.$fname);
                $img->save($path);

                if(filled($obj->file)){
                    $file_to_remove = $obj->file;
                    $obj->file()
                    ->dissociate()
                    ->saveQuietly();
                    //apaga o item para forçar o disparo do observer
                    ZFile::destroy($file_to_remove->id);
                }

                if(filled($img)){
                    $f = ZFile::create([
                        "name"=>$fname,
                        "file_path"=>'zorro/images/'.$fname
                    ]);
                    $obj
                    ->file()
                    ->associate($f)
                    ->saveQuietly();
                }

            }
            else{
                $obj->file()
                ->dissociate()
                ->saveQuietly();
                if(filled(optional(optional($obj)->file)->id)){
                    ZFile::destroy($obj->file->id);
                }
            }
        }
        else{
        }
        return $this;
    }    
}
