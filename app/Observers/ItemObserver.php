<?php

namespace App\Observers;

use App\Models\Item;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\Models\File as ZFile;
use Illuminate\Support\Str;

class ItemObserver
{

    public function created(Item $obj) {
        $this->manageFile($obj);
    }

    public function updated(Item $obj) {
        $this->manageFile($obj);
    }

    public function deleted(Item $obj) {
        if(filled($obj->file)){
            ZFile::destroy($obj->file->id);
            // ->delete();
        }
    }

    public function manageFile(Item $obj){
        $file = $obj->getAppend('__file');
        
        //cria e vincula o arquivo
        if(filled($file)){
            // remove se existe outro arquivo previo
        if(filled($obj->file)){
            $file_to_remove = $obj->file;
            $obj->file()
            ->dissociate()
            ->saveQuietly();
            //apaga o item para forçar o disparo do observer
            ZFile::destroy($file_to_remove->id);
        }

            $fext = $file->guessExtension();
            $fname = Str::slug($obj->name, '-')."-{$obj->id}.{$fext}";

            $path = Storage::putFileAs(
                'zorro/files', $file, $fname
            );

            if(filled($path)){
                $f = ZFile::create([
                    "name"=>$fname,
                    "file_path"=>$path
                ]);
                $obj
                ->file()
                ->associate($f)
                ->saveQuietly();
            }
        }
        return $this;
    }

}
