<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;

class CategoryController extends Controller
{
    public function list($cityId=null){
        
        if(filled($cityId))
            $list = Category::where('city_id',$cityId)
            ->with('category')
            ->get();
        else
            $list = Category::whereNull('city_id')
            ->with('category')
            ->get();

        return response()->json([
            "success"=>true,
            "data"=>$list
        ]); 
    }
}
