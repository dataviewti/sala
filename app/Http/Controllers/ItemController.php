<?php

namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ItemRequest;
use App\Models\Item;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ItemController extends BaseController
{
    public function store(ItemRequest $request){
        $data = (object) $request->all();
        $obj = new Item((array) $data);

        $obj->setAppend('__file',optional($data)->file);

        $res = $obj->save();
        if(filled(optional($data)->categories)) {
            $obj->categories()
                ->sync($data->categories);
        }

        return response()->json([
            "success"=>$res,
            "data"=>$obj
        ]); 
    }

    public function update(ItemRequest $request){
        $new = (object) $request->all();

        $old = Item::find($new->updateId);

        $old->setAppend('__file',optional($new)->file)
            ->touch();

        $update = $old->update((array) $new);

        if(filled(optional($new)->categories)){
            $old->categories()
            ->sync($new->categories);
        }

        return response()->json([
            "success"=>$update,
            "data"=>$old
        ]); 
    }
    
    public function list(){
        $list = Item::select()
        ->with([
            'city',
            'categories',
           'file'
        ])
        ->orderBy('id','desc')
        ->get();

        return response()->json([
            "success"=>true,
            "data"=>$list
        ]); 
    }

    public function edit($id){
        $city = Item::where('id',$id)
        ->with([
            'city',
            'categories',
            'file',
        ])->first();

        return response()->json([
            "success"=>filled($city),
            "data"=>$city
        ]); 
    }  

    public function syncCategories(Request $request){
        $data = (object) $request->all();
        $arr = [];
        foreach($data->data as $c)
            data_set($arr,$c,["main"=>$c===$data->main]);

        Item::find($data->itemId)
            ->categories()
            ->sync($arr);

        return response()->json([
            "success"=>true,
            "data"=>[]
        ]); 
    }     

    public function delete(Request $req){
        $r = (object) collect([
            "force"=>true
        ])
        ->merge($req->all())
        ->all();
        
        $obj = Item::where('id',$r->id)->first();

        //verificar o tipo de user, e adicionar uma verificação secundária
        //repetir essa ação para os IdCity sempre

        if(filled($obj)){
            $del = $r->force ? $obj->forceDelete() : $obj->delete();
        }

        return response()->json([
            "success"=>$del,
            "data"=>[]
        ]); 
    }  
}
