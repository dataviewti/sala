<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function permissions() {
        if(Auth::check()){
            return response()->json([
                    "success"=>true,
                    "data"=>Auth::user()->getPermissions()
            ]);
        }
        return response()->json(["success"=>false,"data"=>[]]);
    }
}
