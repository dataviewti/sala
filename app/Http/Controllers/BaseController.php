<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App;
use Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{
    // protected $redirectTo = '/customer';
    
    protected $class = null;
    protected $customer   = null;

    // protected function guard()
    // {
    //     return Auth::guard('admin');
    // }

    public function __construct() {
      // $this->middleware('auth:admin');

      $strClass = $this->class;
      if(empty($this->class))
        $strClass = "App\\Models\\".str_replace('Controller',"",(new \ReflectionClass(get_class($this)))->getShortName()); 
      $this->class = class_exists($strClass) ? new $strClass : null;
    }

    public function edit($id) { 
      $c = $this->class::find($id);   
      return response()->json(['success'=>filled($c),"data"=>$c]);
    }

    // public function delete($id=null,Request $request){
    //   $data = (Object) $request->all();
    //   $_id = $id ?? optional($data)->deleteId;   

    //   if(filled($_id)){
    //     $obj = $this->class::firstWhere($this->class->getPrimaryKey(),$_id);

    //     $res = filled($obj) ? (optional($data)->force == true ? $obj->forceDelete() : $obj->delete()) : false;
  
    //     return response()->json(['success'=>$res,"data"=>[],"msg"=>__('Deleted with success')]);
    //   }
    //   else
    //     return response()->json(['success'=>false,"msg"=>__('delete fail')]);
    // }
}