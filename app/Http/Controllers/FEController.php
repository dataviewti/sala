<?php

namespace App\Http\Controllers;
use App\Models\Item;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Storage;

class FEController extends Controller
{
    public function download($file) {
		$item = Item::whereHas('file', function (Builder $query) use ($file){
			$query->whereName($file);
		})
		->first();

		if(filled($item)){
			$item->file->counterIncrease();
			return Storage::disk('public')->download($item->file->file_path,$file);
		}
		else
			return abort(404);
    }
}
