<?php

namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CityRequest;
use App\Models\City;
use Illuminate\Http\Request;

class CityController extends BaseController
{

    public function store(CityRequest $request){
        $data = (object) $request->all();
        $city = new City((array) $data);

        $city->setAppend('__resp',optional($data)->resp);
        $city->setAppend('__file',optional($data)->banner);
        
        $res = $city->save();

        $city->setAppend('__file',optional($data)->banner)
        ->touch();

        return response()->json([
            "success"=>$res,
            "data"=>$city
        ]);
    }

    public function update(CityRequest $request){


        $new = (object) $request->all();
        
        $old = City::find($new->updateId);
        
        $update = $old->update((array) $new);
        //apenas se o resp enviado for novo

    
        $old->setAppend('__resp',optional($new)->resp)
        ->setAppend('__file',optional($new)->banner)
        ->touch();

        return response()->json([
            "success"=>$update,
            "data"=>$old
        ]);
    }
    
    public function list(){
        $list = City::all();
        
        return response()->json([
            "success"=>true,
            "data"=>$list
        ]); 
    }

    public function edit($id){
        $city = City::where('id',$id)
        ->with([
            'mainContact',
            'contacts',
            'file'
        ])
        ->first();

        return response()->json([
            "success"=>filled($city),
            "data"=>$city
        ]); 
    }
    
    public function changeStatus(Request $request){
        $d = (object) $request->all();
        $upd = City::find($d->city_id)
        ->update([
            "status" => $d->status
        ]);


        return response()->json([
            "success"=>$upd,
        ]); 
    }    
}
