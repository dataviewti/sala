<?php
namespace App\Http\Requests;

use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ItemRequest extends BaseRequest
{

  protected function prepareForValidation() {
      $input = $this->all();
      $this->merge([
      ]);
  }

  public function rules(){
    $input = $this->all();
    $updateId = $this->input('updateId',null);

    $rules = [
      'name'=>'bail|required',
      'url'=>'bail|sometimes|required|url',
      'file'=>'bail|sometimes', //|max:2048
    ];
    // mimes:csv,txt,xlx,xls,pdf|max:2048    

    return collect($rules)
    ->all();
  }

  public function messages(){
    return [
    ];
  }
}
