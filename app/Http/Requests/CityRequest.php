<?php
namespace App\Http\Requests;

use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class CityRequest extends BaseRequest
{

  protected function prepareForValidation() {
      $input = $this->all();

      //implementar para as demais requests que tiverem esse problema
      //tem q ser recursiuva
      // foreach($input as $k=>$v) {
      //  if($v === 'false' || $v === 'true')
      //   $this->merge([$k=>$v === 'true']);
      // }

      //converte todos os booleanos "bool" com origem em formData
      //procurar uma forma de detectar se a requisição envia formData e fazer auto
      //dentro do model         
      array_walk_recursive($input,function(&$v,$k){
        $v = ($v === 'false' || $v === 'true') ? $v === 'true' : $v;
      });

      $this->merge($input)
      ->merge([
      ]);
  }

  public function objectToArray($object) {
      if(!is_object($object) && !is_array($object))
          return $object;
      return array_map('objectToArray', (array) $object);
  }

  public function rules(){
    $region= $this->input('region');
    $updateId = $this->input('updateId',null);

    $rules = [
      'name' => ['bail',Rule::unique('cities','name')
      ->ignore($updateId)
      ->where(function ($query) use ($region) {
        return $query->where('region',$region);
      })],
      'region'=>'bail|required',
      'address'=>'bail|required',
      // 'mobile'=>'bail| required',
      'office_hours'=>'bail|required',
    ];
    
    //condicionalmente testar o resp
    if($this->has('resp')){
      $resp = [
        // "resp.name"=>'bail|required', (reativar depois de corrigir o problema dos indices ZSlect)
        'resp.phone'=>'bail|required',
        // "resp.whatsapp_message"=>'bail',
        // "resp.email"=>'bail|required|email'
      ];
    }

    return collect($rules)
    ->merge($resp ?? [])
    ->all();
  }

  public function messages(){
    $city= $this->input('name');
    $region= $this->input('region');

    return [
      "name.unique" => "{$city}/{$region} já existe!",
      // "resp.whatsapp_message.*" => "e agora hozê"
    ];
  }
}
