<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\FEController;
use Illuminate\Support\Str;
use App\Models\City;


Route::get('/', function () {
    return view('fe.sala');
});

Route::get('/documents/download/{file}',
    [FEController::class, 'download']
);



Route::middleware(['auth'])->prefix('dashboard')->group(function () {
    
    Route::get('/', function () {
        return view('admin.dashboard');
    });

    Route::get('/permissions',[DashboardController::class, 'permissions']);

    Route::prefix('cities')->group(function () {
        Route::post('/store',[CityController::class, 'store']);
        Route::post('/update',[CityController::class, 'update']);
        Route::get('/list',[CityController::class, 'list']);
        Route::get('/edit/{id}',[CityController::class, 'edit']);
        Route::post('/change-status',[CityController::class, 'changeStatus']);
    });

    Route::prefix('items')->group(function () {
        Route::post('/store',[ItemController::class, 'store']);
        Route::post('/update',[ItemController::class, 'update']);
        Route::post('/delete',[ItemController::class, 'delete']);
        Route::get('/edit/{id}',[ItemController::class, 'edit']);
        Route::get('/list/{city?}',[ItemController::class, 'list']);
        Route::post('/categories/sync',[ItemController::class, 'syncCategories']);
    });

    Route::prefix('categories')->group(function () {
        Route::get('/list/{id?}',[CategoryController::class, 'list']);
    });    

});

Route::get('/login', function () {
   return view("auth.login");
});

Route::get('/admin', function () {
    return redirect('dashboard');
 });


Route::get('/{city}', function ($city) {

    $_city = Str::lower($city);

    $c = City::whereRaw("LOWER(name) = '${_city}' or short_name = '${_city}'",)
        ->where('status',true)
        ->first();

    $data = (object)[
        "cols" => "col-sm-6 col-lg-3"
    ];
    

    if(filled($c))
        return view('fe.city',["city"=>$c, "data"=>$data]);Route::get('/{city}', function ($city) use ($data) {

    $_city = Str::lower($city);

    $c = City::whereRaw("LOWER(name) = '${_city}'",)
        ->where('status',true)
        ->first();

    if(filled($c))
        return view('fe.city',["city"=>$c, "data"=>$data]);

    return view("fe.errors.404");
});


    return view("fe.errors.404");
});

require __DIR__.'/auth.php';
