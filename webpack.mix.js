const mix = require('laravel-mix')
const path = require('path')
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
// .postCss('resources/css/app.css', 'public/css', [
//     require('postcss-import'),
//     require('tailwindcss'),
//     require('autoprefixer'),
// ]);

mix.webpackConfig({
  resolve: {
    alias: {
      '~': path.resolve(__dirname, 'resources/js'),
      '~services': path.resolve(__dirname, 'resources/js/components/admin/services'),
      '@images/': path.resolve(__dirname, 'resources/images'),
      '@': path.resolve(__dirname, 'resources/scss')
    }
  }

})

mix.js('resources/js/app.js', 'public/js')
  .vue()
  .postCss('resources/css/app.css', 'public/css', [
    require('postcss-import'),
    require('tailwindcss'),
    require('autoprefixer')
  ])
  .sass('resources/scss/z-app.scss', 'public/css')

// mix.postCss('resources/css/fe/custom.css', 'public/css')

// mix completo
mix.sass('resources/css/fe/z-fe.scss', 'public/css')

mix.copy('resources/images', 'public/images')

mix.postCss('resources/css/sala.css', 'public/css')
mix.postCss('resources/css/404.css', 'public/css')

// 404

// FRONT
// mix.copy('resources/assets', 'public/')

// mix.sass('resources/scss/z-app.scss', 'public/css/z-custom.css')
